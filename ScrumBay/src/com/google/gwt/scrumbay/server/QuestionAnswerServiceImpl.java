package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.QuestionAnswerService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class QuestionAnswerServiceImpl extends RemoteServiceServlet implements QuestionAnswerService {

	DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Categoria> allCategories = db.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();

	@Override
	public String deleteAnswer(String oggettoKey, Domanda domanda) {
		String result;

		boolean checkQuestion = false;
		synchronized (allObjects) {
			if (!allObjects.containsKey(oggettoKey)) {
				throw new IllegalArgumentException("Oggetto di riferimento non esistente!");
			}
			Oggetto workObject = allObjects.get(oggettoKey);
			for (Domanda question : workObject.getDomande()) {
				if (question.getDomanda().equals(domanda.getDomanda())) {
					Date scadenza = null;
					try {
						scadenza = new SimpleDateFormat("dd/MM/yyyy").parse(workObject.getDataScadenza());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					synchronized (forSale) {
						LinkedList<Oggetto> forSaleObjects = forSale.get(scadenza);
						for (Oggetto object : forSaleObjects) {
							if (object.getKey().equals(oggettoKey)) {
								for (Domanda questionForSale : object.getDomande()) {
									if (questionForSale.getDomanda().equals(domanda.getDomanda())) {
										Categoria category = allCategories
												.get(object.getCategoria().getNomeCategoria());
										for (Oggetto onSaleCategoryObject : category.getOggetti()) {
											if (onSaleCategoryObject.getKey().equals(oggettoKey)) {
												for (Domanda questionCategory : onSaleCategoryObject.getDomande()) {
													if (questionCategory.getDomanda().equals(domanda.getDomanda())) {
														checkQuestion = true;
														question.eliminaRisposta();
														questionForSale.eliminaRisposta();
														questionCategory.eliminaRisposta();
														allObjects.put(oggettoKey, workObject);
														forSale.put(scadenza, forSaleObjects);
														allCategories.put(
																onSaleCategoryObject.getCategoria().getNomeCategoria(),
																category);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				db.commit();
			}

		}

		if (!checkQuestion) {
			throw new IllegalArgumentException(
					"Domanda di riferimento per l'oggetto " + oggettoKey + " non esistente!");
		} else {
			result = "La risposta per la domanda: " + domanda.getDomanda() + " riferente all'oggetto: " + oggettoKey
					+ " è stata eliminata con successo!";
		}

		return result;
	}

	@Override
	public String deleteQuestion(String oggettoKey, Domanda domanda) {
		String result;

		boolean checkQuestion = false;

		synchronized (allObjects) {
			if (!allObjects.containsKey(oggettoKey)) {
				throw new IllegalArgumentException("Oggetto di riferimento non esistente!");
			}

			Oggetto workObject = allObjects.get(oggettoKey);
			for (int i = 0; i < workObject.getDomande().size(); i++) {
				if (workObject.getDomande().get(i).getDomanda().equals(domanda.getDomanda())) {
					Date scadenza = null;
					try {
						scadenza = new SimpleDateFormat("dd/MM/yyyy").parse(workObject.getDataScadenza());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					synchronized (forSale) {
						LinkedList<Oggetto> forSaleObjects = forSale.get(scadenza);
						for (Oggetto object : forSaleObjects) {
							if (object.getKey().equals(oggettoKey)) {
								for (int j = 0; j < object.getDomande().size(); j++) {
									if (object.getDomande().get(j).getDomanda().equals(domanda.getDomanda())
											&& object.getDomande().get(j).getDomanda()
													.equals(workObject.getDomande().get(i).getDomanda())) {
										synchronized (allCategories) {
											Categoria category = allCategories
													.get(object.getCategoria().getNomeCategoria());
											for (Oggetto onSaleCategoryObject : category.getOggetti()) {
												if (onSaleCategoryObject.getKey().equals(oggettoKey)) {
													for (int k = 0; k < onSaleCategoryObject.getDomande().size(); k++) {
														if (onSaleCategoryObject.getDomande().get(k).getDomanda()
																.equals(domanda.getDomanda())) {
															checkQuestion = true;
															workObject.getDomande().remove(i);
															object.getDomande().remove(j);
															onSaleCategoryObject.getDomande().remove(k);
															allObjects.put(oggettoKey, workObject);
															forSale.put(scadenza, forSaleObjects);
															allCategories.put(allObjects.get(oggettoKey).getCategoria()
																	.getNomeCategoria(), category);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				db.commit();
			}
		}

		if (!checkQuestion) {
			throw new IllegalArgumentException(
					"Domanda di riferimento per l'oggetto " + oggettoKey + " non esistente!");
		} else {
			result = "La domanda: " + domanda.getDomanda() + " riferente all'oggetto: " + oggettoKey
					+ " è stata eliminata con successo!";
		}

		return result;
	}

	@Override
	public String newQuestion(String oggettoKey, Domanda domanda) throws IllegalArgumentException {
		String result;

		synchronized (db) {
			if (!allObjects.containsKey(oggettoKey)) {
				throw new IllegalArgumentException("Oggetto di riferimento non esistente!");
			}

			for (Domanda question : allObjects.get(oggettoKey).getDomande()) {
				if (question.getDomanda().equals(domanda.getDomanda())) {
					throw new IllegalArgumentException(
							"Domanda già esistente! Controlla tra le domande in informazione oggetto!");
				}
			}

			Oggetto object = allObjects.get(oggettoKey);
			object.aggiungiDomanda(domanda);
			allObjects.put(oggettoKey, object);

			Date deadline = null;
			try {
				deadline = new SimpleDateFormat("dd/MM/yyyy").parse(allObjects.get(oggettoKey).getDataScadenza());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			LinkedList<Oggetto> onSaleObjects = forSale.get(deadline);
			for (int i = 0; i < onSaleObjects.size(); i++) {
				if (onSaleObjects.get(i).getKey().equals(oggettoKey)) {
					onSaleObjects.set(i, object);
					forSale.put(deadline, onSaleObjects);
					break;
				}
			}

			Categoria category = allCategories.get(object.getCategoria().getNomeCategoria());
			LinkedList<Oggetto> onSaleCategoryObjects = category.getOggetti();
			for (int i = 0; i < onSaleCategoryObjects.size(); i++) {
				if (onSaleCategoryObjects.get(i).getKey().equals(oggettoKey)) {
					category.sostituisciOggetto(i, object);
					allCategories.put(category.getNomeCategoria(), category);
					break;
				}
			}
		}

		result = "Domanda inviata al venditore. Non resta che aspettare!";
		return result;
	}

	@Override
	public String saveAnswer(String oggettoKey, Domanda domanda, String risposta, UtenteVenditore rispondente)
			throws IllegalArgumentException {
		String result = null;
		synchronized (db) {
			if (!allObjects.containsKey(oggettoKey)) {
				throw new IllegalArgumentException("Oggetto di riferimento non esistente!");
			}
			Oggetto object = allObjects.get(oggettoKey);
			for (Domanda question : object.getDomande()) {
				if (question.getDomanda().equals(domanda.getDomanda())) {
					question.setReplica(rispondente, risposta);
					allObjects.put(oggettoKey, object);
					result = "Risposta inserita correttamente!";
					break;
				}
			}

			Date deadline = null;
			try {
				deadline = new SimpleDateFormat("dd/MM/yyyy").parse(allObjects.get(oggettoKey).getDataScadenza());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			LinkedList<Oggetto> onSaleObjects = forSale.get(deadline);
			for (int i = 0; i < onSaleObjects.size(); i++) {
				if (onSaleObjects.get(i).getKey().equals(oggettoKey)) {
					onSaleObjects.set(i, object);
					forSale.put(deadline, onSaleObjects);
					break;
				}
			}

			Categoria category = allCategories.get(object.getCategoria().getNomeCategoria());
			LinkedList<Oggetto> onSaleCategoryObjects = category.getOggetti();
			for (int i = 0; i < onSaleCategoryObjects.size(); i++) {
				if (onSaleCategoryObjects.get(i).getKey().equals(oggettoKey)) {
					category.sostituisciOggetto(i, object);
					allCategories.put(category.getNomeCategoria(), category);
					break;
				}
			}
		}

		return result;
	}
}
