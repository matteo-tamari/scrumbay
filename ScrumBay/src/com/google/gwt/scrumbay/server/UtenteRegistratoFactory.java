package com.google.gwt.scrumbay.server;

import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;

public class UtenteRegistratoFactory extends UtenteFactory {

	public static Utente getUser(String username, String nome, String cognome, String telefono, String password,
			String email, String codiceFiscale, String domicilio, String sesso, String dataNascita,
			String luogoNascita) {
		String none = "Dato non fornito in fase di registrazione";
		if (dataNascita.isEmpty()) {
			dataNascita = none;
		}
		if (luogoNascita.isEmpty()) {
			luogoNascita = none;
		}
		if (sesso.isEmpty()) {
			sesso = none;
		}
		UtenteRegistrato user = new UtenteRegistrato(username, nome, cognome, telefono, password, email, codiceFiscale,
				domicilio, sesso, dataNascita, luogoNascita);
		return user;
	}
}
