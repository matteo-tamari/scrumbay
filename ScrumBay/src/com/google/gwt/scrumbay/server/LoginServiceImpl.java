package com.google.gwt.scrumbay.server;

import com.google.gwt.scrumbay.client.service.LoginService;
import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {

	private static final String SERVICE_MAP = "UtenteRegistrato";

	@Override
	public Utente signIn(String username, String password) throws IllegalArgumentException {
		DB db = DbStarter.getInstance();
		@SuppressWarnings("unchecked")
		HTreeMap<String, Utente> mapUtenti = db.hashMap(SERVICE_MAP, Serializer.STRING, Serializer.JAVA).counterEnable()
				.createOrOpen();

		HTreeMap<String, byte[]> mapSpeziale = db.hashMap("ZonaSpezie", Serializer.STRING, Serializer.BYTE_ARRAY)
				.counterEnable().createOrOpen();

		if (!mapUtenti.containsKey(username)) {
			throw new IllegalArgumentException("Username o password errati");
		} else {

			byte[] salt = mapSpeziale.get(username);
			MessageDigest sha256 = null;
			try {
				sha256 = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			sha256.update(salt);
			String hashedPassword = new String(sha256.digest(password.getBytes(StandardCharsets.UTF_8)));

			Utente utente = mapUtenti.get(username);

			if (utente instanceof UtenteAmministratore) {
				UtenteAmministratore utenteCasted = (UtenteAmministratore) utente;
				if (!utenteCasted.getPassword().equals(hashedPassword)) {
					throw new IllegalArgumentException("Username o password errati");
				}

			} else {
				UtenteRegistrato utenteCasted = (UtenteRegistrato) utente;
				if (!utenteCasted.getPassword().equals(hashedPassword)) {
					throw new IllegalArgumentException("Username o password errati");
				}
			}
		}
		return mapUtenti.get(username);
	}

}
