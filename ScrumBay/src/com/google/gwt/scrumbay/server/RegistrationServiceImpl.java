package com.google.gwt.scrumbay.server;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.shared.Utente;

import com.google.gwt.scrumbay.client.service.RegistrationService;

import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class RegistrationServiceImpl extends RemoteServiceServlet implements RegistrationService {
	private static final String SERVICE_MAP = "UtenteRegistrato";

	@Override
	public String addRegistration(String username, String nome, String cognome, String telefono, String password,
			String email, String codiceFiscale, String domicilio, String sesso, String dataNascita, String luogoNascita)
			throws IllegalArgumentException {
		String result;
		DB db = DbStarter.getInstance();

		@SuppressWarnings("unchecked")
		HTreeMap<String, Utente> mapUtenti = db.hashMap(SERVICE_MAP, Serializer.STRING, Serializer.JAVA).counterEnable()
				.createOrOpen();

		HTreeMap<String, String> mapEmailRegistrate = db
				.hashMap("EmailRegistrate", Serializer.STRING, Serializer.STRING).counterEnable().createOrOpen();

		HTreeMap<String, String> mapCFRegistrati = db.hashMap("CFRegistrati", Serializer.STRING, Serializer.STRING)
				.counterEnable().createOrOpen();

		HTreeMap<String, byte[]> mapSpeziale = db.hashMap("ZonaSpezie", Serializer.STRING, Serializer.BYTE_ARRAY)
				.counterEnable().createOrOpen();

		// Metodo ottenimento chiave da oggeto utente ottenuto in input
		String key = username;

		SecureRandom randomSalting = new SecureRandom();
		byte[] salt = randomSalting.generateSeed(25);
		MessageDigest sha256 = null;
		try {
			sha256 = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		sha256.update(salt);
		String hashedPassword = new String(sha256.digest(password.getBytes(StandardCharsets.UTF_8)));
		password = hashedPassword;
		synchronized (db) {
			if (mapUtenti.containsKey(key))
				throw new IllegalArgumentException("Username utente già utilizzato");

			if (mapEmailRegistrate.containsKey(email))
				throw new IllegalArgumentException("Email utente già utilizzata");

			if (mapCFRegistrati.containsKey(codiceFiscale))
				throw new IllegalArgumentException("Codice fiscale già utilizzato");

			Utente utente = UtenteFactory.getUtente(username, nome, cognome, telefono, password, email, codiceFiscale,
					domicilio, sesso, dataNascita, luogoNascita);

			mapUtenti.put(key, utente);
			mapSpeziale.put(key, salt);
			mapEmailRegistrate.put(email, key);
			if (utente instanceof UtenteRegistrato) {
				mapCFRegistrati.put(codiceFiscale, key);
			}

			db.commit();
		}
		result = "Registrazione completata, grazie per aver scelto ScrumBay!";

		return result;
	}
}
