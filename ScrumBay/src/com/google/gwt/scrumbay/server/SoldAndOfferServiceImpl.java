package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.SoldAndOfferService;
import com.google.gwt.scrumbay.shared.Offerta;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class SoldAndOfferServiceImpl extends RemoteServiceServlet implements SoldAndOfferService {

	private static final String SERVICE_MAP = "OggettiInVendita";
	private DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> mapOggettiInVendita = db.hashMap(SERVICE_MAP, Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();

	@Override
	public LinkedList<Oggetto> getMySoldObjects(UtenteRegistrato user) throws IllegalArgumentException {
		LinkedList<Oggetto> myObjects = new LinkedList<Oggetto>();
		for (Oggetto object : mapOggettiInVendita.getValues()) {
			if (object.getVenditore().getUsername().equals(user.getUsername())) {
				myObjects.add(object);
			}
		}
		return sortObjects(myObjects);
	}

	@Override
	public LinkedList<Oggetto> getObjectsWithMyBid(UtenteRegistrato user) throws IllegalArgumentException {
		LinkedList<Oggetto> myOffers = new LinkedList<Oggetto>();
		for (Oggetto object : mapOggettiInVendita.getValues()) {
			for (Offerta offer : object.getOfferte()) {
				if (offer.getAcquirente().getUsername().equals(user.getUsername())) {
					myOffers.add(object);
					break;
				}
			}
		}
		return sortObjects(myOffers);
	}

	/**
	 * Sorts the objects in date ascending order
	 * 
	 * @param objectsToSort list
	 * @return objectsToSort sorted list
	 */
	private LinkedList<Oggetto> sortObjects(LinkedList<Oggetto> objectsToSort) {
		Collections.sort(objectsToSort, new Comparator<Oggetto>() {
			@Override
			public int compare(Oggetto arg0, Oggetto arg1) {
				Date date1 = null;
				Date date2 = null;
				try {
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(arg0.getDataScadenza());
					date2 = new SimpleDateFormat("dd/MM/yyyy").parse(arg1.getDataScadenza());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return date1.compareTo(date2);
			}
		});
		return objectsToSort;
	}
}
