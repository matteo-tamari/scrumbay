package com.google.gwt.scrumbay.server;

import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;

public class UtenteFactory {

	public static Utente getUtente(String username, String nome, String cognome, String telefono, String password,
			String email, String codiceFiscale, String domicilio, String sesso, String dataNascita,
			String luogoNascita) {
		Utente utente;

		if (cognome.isEmpty() && nome.isEmpty() && telefono.isEmpty() && codiceFiscale.isEmpty() && domicilio.isEmpty()
				&& sesso.isEmpty() && dataNascita.isEmpty() && luogoNascita.isEmpty()) {
			System.out.println(username);
			utente = new UtenteAmministratore(username, password, email);
		} else {
			utente = UtenteRegistratoFactory.getUser(username, nome, cognome, telefono, password, email, codiceFiscale,
					domicilio, sesso, dataNascita, luogoNascita);
		}

		return utente;
	}
}
