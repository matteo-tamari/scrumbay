package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.CategoryService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class CategoryServiceImpl extends RemoteServiceServlet implements CategoryService {

	private static final String SERVICE_MAP_CATEGORIA = "CategorieOggetti";
	private static DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private static HTreeMap<String, Categoria> mapCategorie = db
			.hashMap(SERVICE_MAP_CATEGORIA, Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> mapOggettiInVendita = db
			.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<Date, LinkedList<Oggetto>> dayOrdered = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();

	@Override
	public String addNewCategory(String categoriaPadre, String nuovaCategoria) throws IllegalArgumentException {
		String result;
		synchronized (db) {
			if (mapCategorie.containsKey(nuovaCategoria)) {
				throw new IllegalArgumentException("Nome categoria inserito gia'  esistente!");
			}

			if (mapCategorie.getSize() == 0) {
				mapCategorie.put("NULL", new Categoria("NULL", new Categoria("NULL")));
			}

			if (!mapCategorie.containsKey(categoriaPadre)) {
				throw new IllegalArgumentException("Categoria padre di riferimento inesistente!");
			}

			Categoria aggiornamentoCategoria = mapCategorie.get(categoriaPadre);
			Categoria categoria = new Categoria(nuovaCategoria, aggiornamentoCategoria);
			aggiornamentoCategoria.setSottoCategorie(categoria);
			categoria.setCategoriaPadre(aggiornamentoCategoria);
			mapCategorie.put(categoriaPadre, aggiornamentoCategoria);
			mapCategorie.put(nuovaCategoria, categoria);
			db.commit();
		}
		result = "Nuova categoria: " + nuovaCategoria + " aggiunta correttamente all'elenco delle categorie!";
		return result;
	}

	@Override
	public LinkedList<Categoria> getCategories(String categoriaPadre) throws IllegalArgumentException {
		if (mapCategorie.getSize() == 0) {
			return new LinkedList<Categoria>();
		}
		if (!mapCategorie.containsKey(categoriaPadre)) {
			throw new IllegalArgumentException("Categoria padre di riferimento inesistente!");
		}
		return mapCategorie.get(categoriaPadre).getSottoCategorie();
	}

	@Override
	public String renameCategory(String nomeCategoria, String nuovoNomeCategoria) throws IllegalArgumentException {
		String result;
		synchronized (db) {
			if (!mapCategorie.containsKey(nomeCategoria)) {
				throw new IllegalArgumentException("Nome categoria da modificare inesistente!");
			}

			if (mapCategorie.containsKey(nuovoNomeCategoria)) {
				throw new IllegalArgumentException("Nuovo nome categoria inserito gia'  esistente!");
			}

			// Rinomino la categoria
			Categoria categoria = mapCategorie.get(nomeCategoria);
			categoria.setNomeCategoria(nuovoNomeCategoria);
			// Rimappo le categorie figlie
			for (Categoria categoriaFiglia : categoria.getSottoCategorie()) {
				categoriaFiglia = mapCategorie.get(categoriaFiglia.getNomeCategoria());
				categoriaFiglia.setCategoriaPadre(categoria);
				mapCategorie.put(categoriaFiglia.getNomeCategoria(), categoriaFiglia);
			}
			mapCategorie.put(nuovoNomeCategoria, categoria);
			// Rimappo la categoria padre di quella modificata
			Categoria categoriaPadre = mapCategorie.get(categoria.getCategoriaPadre().getNomeCategoria());
			categoriaPadre.sostituisciSottoCategoria(mapCategorie.get(nomeCategoria),
					mapCategorie.get(nuovoNomeCategoria));
			mapCategorie.put(categoriaPadre.getNomeCategoria(), categoriaPadre);
			mapCategorie.remove(nomeCategoria);
			// Modifico categoria dentro gli oggetti a lei appartenenti
			for (Oggetto object : categoria.getOggetti()) {
				object.setCategoria(categoria);
				// Modifico categoria dentro la collezione di oggetti venduti da sempre
				mapOggettiInVendita.put(object.getKey(), object);
				Date date = null;
				try {
					date = new SimpleDateFormat("dd/MM/yyyy").parse(object.getDataScadenza());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				LinkedList<Oggetto> onSale = dayOrdered.get(date);
				for (int i = 0; i < onSale.size(); i++) {
					// Modifico categoria dentro gli oggetti attualmente in vendita
					Oggetto onSaleObject = onSale.get(i);
					if (onSaleObject.getKey().equals(object.getKey())) {
						onSale.set(i, object);
						dayOrdered.put(date, onSale);
						break;
					}
				}
			}
			mapCategorie.put(categoria.getNomeCategoria(), categoria);
			db.commit();
		}
		result = "Categoria " + nomeCategoria + " rinominata correttamente in " + nuovoNomeCategoria;
		return result;
	}

}
