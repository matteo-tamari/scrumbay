package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.OnSaleService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.StatoVendita;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class OnSaleServiceImpl extends RemoteServiceServlet implements OnSaleService {
	private DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	private HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();
	private Date today = null;

	@Override
	public LinkedList<Oggetto> getOnSaleObjects() {
		today = new Date();
		LinkedList<Oggetto> returnList = null; // La lista di oggetti attualmente in vendita
		synchronized (db) {
			LinkedList<Date> pastDates = getLastRefresh();
			if (pastDates.size() > 0) {
				// Per ogni data passata
				for (Date pastDate : pastDates) {
					// Per ogni oggetto della lista delle scadenze di quel giorno
					for (Oggetto expiredItem : forSale.get(pastDate)) {
						// Si conclude l'asta
						expiredItem.setStato(StatoVendita.ASTACONCLUSA);
						// Si salva nel db contenente tutti gli oggetti venduti dal principio
						allObjects.put(expiredItem.getKey(), expiredItem);
						// Si rimuove l'oggetto dal db delle categorie
						Categoria cathegory = mapCategorie.get(expiredItem.getCategoria().getNomeCategoria());
						LinkedList<Oggetto> objectsInCathegory = cathegory.getOggetti();
						for (int i = 0; i < objectsInCathegory.size(); i++) {
							if (objectsInCathegory.get(i).getKey().equals(expiredItem.getKey())) {
								objectsInCathegory.remove(i);
								mapCategorie.put(cathegory.getNomeCategoria(), cathegory);
								break;
							}
						}
					}
					// Si rimuove dal db degli oggetti attualmente in vendita gli oggetti
					// scaduti
					forSale.remove(pastDate);
				}
			}
			returnList = new LinkedList<Oggetto>();
			for (Date futureDate : forSale.getKeys()) {
				for (Oggetto oggetto : forSale.get(futureDate)) {
					returnList.add(oggetto);
				}
			}
			db.commit();
		}
		return sortObjects(returnList);
	}

	private LinkedList<Date> getLastRefresh() {
		LinkedList<Date> pastDates = new LinkedList<Date>();
		for (Date key : forSale.getKeys()) {
			if (today.after(key)) {
				pastDates.add(key);
			}
		}
		return pastDates;
	}

	public LinkedList<Oggetto> getOnSaleObjectsForCategory(String categoriaPadre) {
		LinkedList<Categoria> sottocategorie = null;
		LinkedList<Oggetto> returnList = new LinkedList<Oggetto>();
		synchronized (mapCategorie) {
			if (mapCategorie.get(categoriaPadre) == null) {
				throw new IllegalArgumentException("Questa categoria non esiste");
			}
			returnList.addAll(mapCategorie.get(categoriaPadre).getOggetti());
			sottocategorie = mapCategorie.get(categoriaPadre).getSottoCategorie();
		}
		for (Categoria categoria : sottocategorie) {
			returnList = subCategoryExplorer(categoria, returnList);
		}
		return sortObjects(returnList);
	}

	private LinkedList<Oggetto> subCategoryExplorer(Categoria subCategory, LinkedList<Oggetto> objectList) {
		Categoria sottocategoria = mapCategorie.get(subCategory.getNomeCategoria());
		objectList.addAll(sottocategoria.getOggetti());
		for (Categoria category : sottocategoria.getSottoCategorie()) {
			objectList = subCategoryExplorer(category, objectList);
		}
		return objectList;
	}

	/**
	 * Sorts the objects in date ascending order
	 * 
	 * @param objectsToSort list
	 * @return objectsToSort sorted list
	 */
	private LinkedList<Oggetto> sortObjects(LinkedList<Oggetto> objectsToSort) {
		Collections.sort(objectsToSort, new Comparator<Oggetto>() {
			@Override
			public int compare(Oggetto arg0, Oggetto arg1) {
				Date date1 = null;
				Date date2 = null;
				try {
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(arg0.getDataScadenza());
					date2 = new SimpleDateFormat("dd/MM/yyyy").parse(arg1.getDataScadenza());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return date1.compareTo(date2);
			}
		});
		return objectsToSort;
	}

}
