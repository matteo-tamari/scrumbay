package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.RemoveObjectService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class RemoveObjectServiceImpl extends RemoteServiceServlet implements RemoveObjectService {

	private DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();

	@Override
	public String removeObject(String objectKey) throws IllegalArgumentException {
		Date date = null;
		Oggetto objectToRemove = allObjects.get(objectKey);
		synchronized (allObjects) {
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(allObjects.get(objectKey).getDataScadenza());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			allObjects.remove(objectKey);
		}
		synchronized (forSale) {
			LinkedList<Oggetto> oggetti = forSale.get(date);
			for (int i = 0; i < oggetti.size(); i++) {
				if (objectKey.equals(oggetti.get(i).getKey())) {
					oggetti.remove(i);
					forSale.put(date, oggetti);
					break;
				}
			}
		}
		synchronized (mapCategorie) {
			Categoria cathegory = mapCategorie.get(objectToRemove.getCategoria().getNomeCategoria());
			for (int i = 0; i < cathegory.getOggetti().size(); i++) {
				if (cathegory.getOggetti().get(i).getKey().equals(objectToRemove.getKey())) {
					cathegory.getOggetti().remove(i);
					mapCategorie.put(cathegory.getNomeCategoria(), cathegory);
					break;
				}
			}
			db.commit();
		}
		return "Rimozione oggetto completata";
	}

}
