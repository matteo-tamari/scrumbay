package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.SaleService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class SaleServiceImpl extends RemoteServiceServlet implements SaleService {

	private static final String SERVICE_MAP = "OggettiInVendita";
	DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> mapOggettiInVendita = db.hashMap(SERVICE_MAP, Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<Date, LinkedList<Oggetto>> dayOrdered = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();

	@Override
	public String sellItem(UtenteVenditore venditore, String nome, String descrizione, double prezzoBase,
			String dataScadenza, Categoria categoria) throws IllegalArgumentException {
		Date today = new Date();
		Oggetto oggettoInVendita = OggettoFactory.getOggetto(venditore, nome, descrizione, prezzoBase, dataScadenza,
				categoria);
		String key = oggettoInVendita.getKey();
		Date scadenza = null;
		try {
			scadenza = new SimpleDateFormat("dd/MM/yyyy").parse(dataScadenza);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (scadenza.after(today)) {
			synchronized (mapOggettiInVendita) {
				if (!mapOggettiInVendita.containsKey(key)) {
					mapOggettiInVendita.put(key, oggettoInVendita);
				} else {
					throw new IllegalArgumentException("Qualcuno ha gia' messo in vendita un oggetto con questo nome.");
				}
			}
			synchronized (dayOrdered) {
				LinkedList<Oggetto> dayList = null;
				if (dayOrdered.containsKey(scadenza)) {
					dayList = dayOrdered.get(scadenza);
				} else {
					dayList = new LinkedList<Oggetto>();
				}
				dayList.add(oggettoInVendita);
				dayOrdered.put(scadenza, dayList);
			}
			synchronized (mapCategorie) {
				Categoria cathegory = mapCategorie.get(oggettoInVendita.getCategoria().getNomeCategoria());
				cathegory.aggiungiOggetto(oggettoInVendita);
				mapCategorie.put(cathegory.getNomeCategoria(), cathegory);
				db.commit();
			}
		} else {
			throw new IllegalArgumentException("Data di scadenza non permessa");
		}
		return "Oggetto aggiunto all'elenco in vendita, \n grazie per aver scelto Scrumbay!";
	}

}
