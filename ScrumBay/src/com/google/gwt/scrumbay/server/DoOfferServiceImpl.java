package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.DoOfferService;
import com.google.gwt.scrumbay.shared.Offerta;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class DoOfferServiceImpl extends RemoteServiceServlet implements DoOfferService {

	private static final String SERVICE_MAP = "OggettiInVendita";
	DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Oggetto> mapOggettiInVendita = db.hashMap(SERVICE_MAP, Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	HTreeMap<Date, LinkedList<Oggetto>> dayOrdered = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();

	@Override
	public String offer(UtenteAcquirente acquirente, double importo, Oggetto oggetto) throws IllegalArgumentException {
		Date day = null;
		try {
			day = new SimpleDateFormat("dd/MM/yyyy").parse(oggetto.getDataScadenza());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		synchronized (db) {
			oggetto = mapOggettiInVendita.get(oggetto.getKey());
			if (oggetto.getNumeroOfferte() > 0) {
				if (oggetto.getMiglioreOfferta().getImporto() < importo) {
					Offerta offerta = new Offerta(acquirente, importo);
					oggetto.aggiungiOfferta(offerta);
					mapOggettiInVendita.put(oggetto.getKey(), oggetto);
				} else {
					throw new IllegalArgumentException("La tua offerta e' inferiore alla attuale vincente!");
				}
			} else if (oggetto.getNumeroOfferte() == 0) {
				if (oggetto.getPrezzoBase() <= importo) {
					Offerta offerta = new Offerta(acquirente, importo);
					oggetto.aggiungiOfferta(offerta);
					mapOggettiInVendita.put(oggetto.getKey(), oggetto);
				} else {
					throw new IllegalArgumentException(
							"La tua offerta e' inferiore al prezzo base d'asta per questo oggetto!");
				}
			}
		}
		synchronized (db) {
			LinkedList<Oggetto> list = dayOrdered.get(day);
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getKey().equals(oggetto.getKey())) {
					list.remove(i);
					list.add(i, oggetto);
					break;
				}
			}
			dayOrdered.put(day, list);
			db.commit();
		}
		return "La tua offerta e' la migliore!";
	}

}
