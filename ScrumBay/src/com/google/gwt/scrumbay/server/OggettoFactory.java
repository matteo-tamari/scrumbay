package com.google.gwt.scrumbay.server;

import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.StatoVendita;
import com.google.gwt.scrumbay.shared.UtenteVenditore;

public class OggettoFactory {
	public static Oggetto getOggetto(UtenteVenditore venditore, String nome, String descrizione, double prezzoBase,
			String dataScadenza, Categoria categoria) {
		Oggetto oggettoInVendita = new Oggetto(venditore, nome, descrizione, prezzoBase, dataScadenza, categoria);
		oggettoInVendita.setStato(StatoVendita.ASTAINCORSO);
		return oggettoInVendita;
	}
}
