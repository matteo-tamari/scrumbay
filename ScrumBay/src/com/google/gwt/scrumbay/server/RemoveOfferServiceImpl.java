package com.google.gwt.scrumbay.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.client.service.RemoveOfferService;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class RemoveOfferServiceImpl extends RemoteServiceServlet implements RemoveOfferService {

	DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();

	@Override
	public String removeOffer(String objectKey) {
		Oggetto oggetto = null;
		synchronized (allObjects) {
			oggetto = allObjects.get(objectKey);
			oggetto.rimuoviOffertaMigliore();
			allObjects.put(objectKey, oggetto);
		}
		synchronized (forSale) {
			Date date = null;
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(oggetto.getDataScadenza());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			LinkedList<Oggetto> oggetti = forSale.get(date);
			for (int i = 0; i < oggetti.size(); i++) {
				if (oggetti.get(i).getKey().equals(oggetto.getKey())) {
					oggetti.set(i, oggetto);
					break;
				}
			}
			forSale.put(date, oggetti);
		}
		synchronized (mapCategorie) {
			Categoria cathegory = mapCategorie.get(oggetto.getCategoria().getNomeCategoria());
			for (int i = 0; i < cathegory.getOggetti().size(); i++) {
				if (cathegory.getOggetti().get(i).getKey().equals(oggetto.getKey())) {
					cathegory.getOggetti().get(i).rimuoviOffertaMigliore();
					mapCategorie.put(cathegory.getNomeCategoria(), cathegory);
					break;
				}
			}
			db.commit();
		}
		return "Rimozione offerta effettuata";
	}

}
