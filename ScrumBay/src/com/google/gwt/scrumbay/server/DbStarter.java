package com.google.gwt.scrumbay.server;

import org.mapdb.DBMaker;
import org.mapdb.DB;

public final class DbStarter {

	private static final String DB_FILE = "ScrumBay.db";
	private static final DbStarter STARTER = new DbStarter();
	private DB single_istance = null;

	private DbStarter() {
		single_istance = DBMaker.fileDB(DB_FILE).transactionEnable().make();
	}

	public static DB getInstance() {
		return STARTER.single_istance;
	}
}