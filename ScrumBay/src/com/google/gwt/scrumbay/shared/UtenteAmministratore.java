package com.google.gwt.scrumbay.shared;

public final class UtenteAmministratore implements Utente {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6450456439354074297L;

	private String username;
	private String password;
	private String email;

	public UtenteAmministratore() {
	}

	public UtenteAmministratore(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

}
