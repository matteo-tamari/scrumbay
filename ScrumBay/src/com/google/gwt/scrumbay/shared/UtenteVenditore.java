package com.google.gwt.scrumbay.shared;

public final class UtenteVenditore extends UtenteRegistrato {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2469282405031083710L;

	public UtenteVenditore() {
	}

	public UtenteVenditore(UtenteRegistrato utente) {
		super(utente.getUsername(), utente.getNome(), utente.getCognome(), utente.getTelefono(), utente.getPassword(),
				utente.getEmail(), utente.getCodiceFiscale(), utente.getDomicilio(), utente.getSesso(),
				utente.getDataNascita(), utente.getLuogoNascita());
	}
}
