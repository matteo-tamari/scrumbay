package com.google.gwt.scrumbay.shared;

import java.util.LinkedList;
import java.io.Serializable;

public class Oggetto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7679403663265409272L;

	private UtenteVenditore venditore;
	private String nome;
	private String descrizione;
	private double prezzoBase;
	private String dataScadenza;
	private Categoria categoria;
	private LinkedList<Domanda> domande;
	private LinkedList<Offerta> offerte;
	private String stato;

	public Oggetto() {
	}

	public Oggetto(UtenteVenditore venditore, String nome, String descrizione, double prezzoBase, String dataScadenza,
			Categoria categoria) {
		this.venditore = venditore;
		this.nome = nome;
		this.descrizione = descrizione;
		this.prezzoBase = prezzoBase;
		this.dataScadenza = dataScadenza;
		this.categoria = categoria;
		this.domande = new LinkedList<Domanda>();
		this.offerte = new LinkedList<Offerta>();
	}

	public void aggiungiDomanda(Domanda domanda) {
		domande.add(domanda);
	}

	/**
	 * Aggiunge una offerta e setta le altre come perdenti
	 * 
	 * @param offerta
	 */
	public void aggiungiOfferta(Offerta offerta) {
		if (offerte.size() > 0) {
			for (Offerta offertaI : offerte) {
				offertaI.setStato(StatoOfferta.PERDENTEINCORSO);
			}
		}
		offerte.add(offerta);
		offerta.setStato(StatoOfferta.VINCENTEINCORSO);
	}

	/**
	 * Setta lo stato dell'asta, in caso lo stato sia "ASTACONCLUSA" le offerte
	 * precedenti vengono contrassegnate come perdenti e l' ultima come vincente.
	 * 
	 * @param stato
	 */
	public void setStato(StatoVendita stato) {
		this.stato = stato.getStato();
		if (stato.equals(StatoVendita.ASTACONCLUSA)) {
			fineAsta();
		}
	}

	private void fineAsta() {
		if (offerte.size() > 0) {
			for (Offerta offerta : offerte) {
				offerta.setStato(StatoOfferta.OFFERTAPERDENTE);
			}
			offerte.getLast().setStato(StatoOfferta.OFFERTAVINCENTE);
			stato = stato.concat(offerte.getLast().getAcquirente().getUsername());
		} else {
			stato = StatoVendita.ASTAFALLITA.getStato();
		}
	}

	public String getKey() {
		return venditore.getUsername() + nome + dataScadenza;
	}

	public Offerta getMiglioreOfferta() {
		if (offerte.size() <= 0) {
			return new Offerta(null, 0);
		}
		return offerte.getLast();
	}

	public int getNumeroOfferte() {
		return offerte.size();
	}

	public String getStato() {
		return stato;
	}

	public void rimuoviOffertaMigliore() {
		if (offerte.size() > 0) {
			offerte.removeLast();
			offerte.getLast().setStato(StatoOfferta.VINCENTEINCORSO);
		}
	}

	public String getDataScadenza() {
		return dataScadenza;
	}

	public UtenteVenditore getVenditore() {
		return venditore;
	}

	public String getNome() {
		return nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public double getPrezzoBase() {
		return prezzoBase;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public LinkedList<Domanda> getDomande() {
		return domande;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public LinkedList<Offerta> getOfferte() {
		return offerte;
	}

	public LinkedList<Domanda> getDomandePendenti() {
		LinkedList<Domanda> domandePendenti = new LinkedList<Domanda>();
		for (Domanda domanda : domande) {
			if (domanda.getReplica() == null) {
				domandePendenti.add(domanda);
			}
		}
		return domandePendenti;
	}
}
