package com.google.gwt.scrumbay.shared;

import java.io.Serializable;

public enum StatoOfferta implements Serializable {
	OFFERTAVINCENTE("Asta chiusa e offerta migliore"), OFFERTAPERDENTE("Asta chiusa e oggetto non aggiudicato"),
	VINCENTEINCORSO("Asta in corso e offerta migliore"), PERDENTEINCORSO("Asta in corso e offerta superata");

	private String stato;

	private StatoOfferta(String stato) {
		this.stato = stato;
	}

	public String getStato() {
		return stato;
	}
}
