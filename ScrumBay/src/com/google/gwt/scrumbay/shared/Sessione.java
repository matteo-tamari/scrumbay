package com.google.gwt.scrumbay.shared;

public final class Sessione {
	private static final Sessione SESSIONE = new Sessione();
	private Utente utente = null;

	private Sessione() {
		this.utente = new UtenteVisitatore();
	}

	public static Utente getSessioneUtente() {
		return SESSIONE.utente;
	}

	public static void setSessioneUtente(Utente utente) {
		SESSIONE.utente = utente;
	}
}