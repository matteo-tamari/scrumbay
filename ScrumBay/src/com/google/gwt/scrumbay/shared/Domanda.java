package com.google.gwt.scrumbay.shared;

import java.io.Serializable;

public class Domanda implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1510317342318083134L;

	private UtenteAcquirente richiedente;
	private String domanda;
	private Risposta replica;

	public Domanda() {
	}

	public Domanda(String domanda, UtenteAcquirente richiedente) {
		this.domanda = domanda;
		this.richiedente = richiedente;
		this.replica = null;
	}

	public UtenteAcquirente getRichiedente() {
		return richiedente;
	}

	public String getDomanda() {
		return domanda;
	}

	public Risposta getReplica() {
		return replica;
	}

	public void setReplica(UtenteVenditore rispondente, String risposta) {
		replica = new Risposta(rispondente, risposta);
	}

	public void eliminaRisposta() {
		replica = null;
	}
}
