package com.google.gwt.scrumbay.shared;

import java.io.Serializable;
import java.util.LinkedList;

public class Categoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1582826604897451830L;

	private String nome;
	private Categoria categoriaPadre;
	private LinkedList<Categoria> sottoCategorie;
	private LinkedList<Oggetto> oggetti;

	public Categoria() {
	}

	public Categoria(String nome) {
		this.nome = nome;
		categoriaPadre = null;
		sottoCategorie = new LinkedList<Categoria>();
		oggetti = new LinkedList<Oggetto>();
	}

	public Categoria(String nome, Categoria categoriaPadre) {
		this.nome = nome;
		this.categoriaPadre = categoriaPadre;
		sottoCategorie = new LinkedList<Categoria>();
		oggetti = new LinkedList<Oggetto>();
	}

	public void setNomeCategoria(String nome) {
		this.nome = nome;
	}

	public void setCategoriaPadre(Categoria categoriaPadre) {
		this.categoriaPadre = categoriaPadre;
	}

	public void setSottoCategorie(Categoria sottoCategoria) {
		this.sottoCategorie.addLast(sottoCategoria);
	}

	public String getNomeCategoria() {
		return nome;
	}

	public Categoria getCategoriaPadre() {
		return categoriaPadre;
	}

	public LinkedList<Categoria> getSottoCategorie() {
		return this.sottoCategorie;
	}

	public void sostituisciSottoCategoria(Categoria vecchiaCategoria, Categoria nuovaCategoria) {
		for (int i = 0; i < sottoCategorie.size(); i++) {
			if (sottoCategorie.get(i).getNomeCategoria().equals(vecchiaCategoria.getNomeCategoria())) {
				sottoCategorie.remove(i);
				sottoCategorie.add(i, nuovaCategoria);
			}
		}
	}

	public LinkedList<Oggetto> getOggetti() {
		return oggetti;
	}

	public void aggiungiOggetto(Oggetto object) {
		oggetti.add(object);
	}

	public void sostituisciOggetto(int i, Oggetto nuovoOggetto) {
		oggetti.set(i, nuovoOggetto);
	}
}
