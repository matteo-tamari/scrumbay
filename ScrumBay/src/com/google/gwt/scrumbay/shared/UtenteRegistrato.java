package com.google.gwt.scrumbay.shared;

public class UtenteRegistrato implements Utente {

	/**
	* 
	*/
	private static final long serialVersionUID = 8181010808501905121L;

	private String username;
	private String nome;
	private String cognome;
	private String telefono;
	private String password;
	private String email;
	private String codiceFiscale;
	private String domicilio;
	private String sesso;
	private String dataNascita;
	private String luogoNascita;

	public UtenteRegistrato() {
	}

	public UtenteRegistrato(String username, String nome, String cognome, String telefono, String password,
			String email, String codiceFiscale, String domicilio, String sesso, String dataNascita,
			String luogoNascita) {
		super();
		this.username = username;
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
		this.password = password;
		this.email = email;
		this.codiceFiscale = codiceFiscale;
		this.domicilio = domicilio;
		this.sesso = sesso;
		this.dataNascita = dataNascita;
		this.luogoNascita = luogoNascita;
	}

	public String getUsername() {
		return this.username;
	}

	public String getEmail() {
		return this.email;
	}

	public String getCodiceFiscale() {
		return this.codiceFiscale;
	}

	public String getNome() {
		return this.nome;
	}

	public String getCognome() {
		return this.cognome;
	}

	public String getPassword() {
		return this.password;
	}

	public String getDomicilio() {
		return this.domicilio;
	}

	public String getDataNascita() {
		return this.dataNascita;
	}

	public String getLuogoNascita() {
		return this.luogoNascita;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public String getSesso() {
		return this.sesso;
	}

}
