package com.google.gwt.scrumbay.shared;

public final class UtenteAcquirente extends UtenteRegistrato {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4511573393435605133L;

	public UtenteAcquirente() {
	}

	public UtenteAcquirente(UtenteRegistrato utente) {
		super(utente.getUsername(), utente.getNome(), utente.getCognome(), utente.getTelefono(), utente.getPassword(),
				utente.getEmail(), utente.getCodiceFiscale(), utente.getDomicilio(), utente.getSesso(),
				utente.getDataNascita(), utente.getLuogoNascita());
	}
}
