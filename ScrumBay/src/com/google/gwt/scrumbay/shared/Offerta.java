package com.google.gwt.scrumbay.shared;

import java.io.Serializable;

public class Offerta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5085034287258360806L;

	private UtenteAcquirente acquirente;
	private double importo;
	private String stato;

	public Offerta() {
		this.acquirente = null;
		this.importo = 0;
	}

	public Offerta(UtenteAcquirente acquirente, double importo) {
		this.acquirente = acquirente;
		this.importo = importo;
	}

	public void setStato(StatoOfferta stato) {
		this.stato = stato.getStato();
	}

	public UtenteAcquirente getAcquirente() {
		return acquirente;
	}

	public double getImporto() {
		return importo;
	}

	public String getStato() {
		return stato;
	}
}
