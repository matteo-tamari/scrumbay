package com.google.gwt.scrumbay.shared;

import java.io.Serializable;

public class Risposta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3756425938805921378L;

	private UtenteVenditore rispondente;
	private String risposta;

	public Risposta() {
	}

	public Risposta(UtenteVenditore rispondente, String risposta) {
		this.rispondente = rispondente;
		this.risposta = risposta;
	}

	public UtenteVenditore getRispondente() {
		return this.rispondente;
	}

	public String getRisposta() {
		return this.risposta;
	}
}
