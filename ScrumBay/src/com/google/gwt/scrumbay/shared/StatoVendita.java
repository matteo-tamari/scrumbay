package com.google.gwt.scrumbay.shared;

import java.io.Serializable;

public enum StatoVendita implements Serializable {
	ASTAINCORSO("Asta in corso"), ASTACONCLUSA("Asta conclusa e oggetto aggiudicato da: "),
	ASTAFALLITA("Asta conclusa, oggetto non aggiudicato");

	private String stato;

	private StatoVendita(String stato) {
		this.stato = stato;
	}

	public String getStato() {
		return stato;
	}
}
