package com.google.gwt.scrumbay.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.RegistrationService;
import com.google.gwt.scrumbay.client.service.RegistrationServiceAsync;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ScrumBay implements EntryPoint {

	@Override
	public void onModuleLoad() {

		// INSERIMENTO NEL DB ALL'AVVIO DI UTENTE ADMIN
		RegistrationServiceAsync registrazioneAdmin = GWT.create(RegistrationService.class);
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			@Override
			public void onSuccess(String result) {
				System.out.println(result);
			}
		};
		registrazioneAdmin.addRegistration("admin", "", "", "", "admin", "admin@gmail.com", "", "", "", "", "",
				callback);

		final SimpleEventBus eventBus = new SimpleEventBus();
		@SuppressWarnings("unused")
		final AppController appViewer = new AppController(eventBus);
		eventBus.fireEvent(new ShowMainPageEvent());
	}

}
