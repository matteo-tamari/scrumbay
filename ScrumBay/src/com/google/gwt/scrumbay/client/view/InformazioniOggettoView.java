package com.google.gwt.scrumbay.client.view;

import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.scrumbay.client.presenter.InformazioniOggettoPresenter;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.scrumbay.shared.UtenteVenditore;

public class InformazioniOggettoView implements InformazioniOggettoPresenter.Display {

	private Grid informationPanel;
	private VerticalPanel rootPanel;
	private Label campoNomeOggetto;
	private Label campoPrezzoVendita;
	private Label campoDescrizioneOggetto;
	private Label campoDataScadenza;
	private Label campoCategoria;
	private Label campoVenditore;
	private Label campoMaxOffertaAttuale;
	private Label etichettaNomeOggetto;
	private Label etichettaDescrizioneOggetto;
	private Label etichettaPrezzoVendita;
	private Label etichettaDataScadenza;
	private Label etichettaCategoria;
	private Label etichettaVenditore;
	private Label etichettaMaxOffertaAttuale;
	private Label etichettaDomandeRisposte;
	private Label campoUtenteVincitore;
	private Label etichettaUtenteVincitore;

	private VerticalPanel offertaOggetto;
	private VerticalPanel domandaOggetto;
	private CellList<String> domandeRisposte;
	private SimplePager pager;
	private VerticalPanel rispostaOggetto;

	public InformazioniOggettoView() {
		informationPanel = new Grid(10, 5);
		rootPanel = new VerticalPanel();
		offertaOggetto = new VerticalPanel();
		offertaOggetto.setStyleName("offerta");
		domandaOggetto = new VerticalPanel();
		domandaOggetto.setStyleName("offerta");
		rispostaOggetto = new VerticalPanel();
		rispostaOggetto.setStyleName("offerta");

		domandeRisposte = new CellList<String>(new TextCell());
		domandeRisposte.setStyleName("col-md-10 offset-md-1 mt-3");
		domandeRisposte.setStyleName("layoutDomandeRisposte");
		pager = new SimplePager();
		pager.setStyleName("col-md-10 offset-md-1 mt-3");

		campoDescrizioneOggetto = new Label();
		campoDescrizioneOggetto.setStyleName("desciption");
		campoDataScadenza = new Label();
		campoDataScadenza.setStyleName("campoDataScadenza");
		campoCategoria = new Label();
		campoCategoria.setStyleName("campoCategoria");
		campoNomeOggetto = new Label();
		campoNomeOggetto.setStyleName("campoNomeOggettoLabel");
		campoPrezzoVendita = new Label();
		campoPrezzoVendita.setStyleName("campoPrezzoVendita");
		campoUtenteVincitore = new Label();
		campoUtenteVincitore.setStyleName("campoPrezzoVendita");
		etichettaUtenteVincitore = new Label("Utente vincitore: ");
		etichettaNomeOggetto = new Label("Nome dell'oggetto: ");
		etichettaDescrizioneOggetto = new Label("Descrizione: ");
		etichettaPrezzoVendita = new Label("Prezzo di partenza: ");
		etichettaDataScadenza = new Label("Data scadenza dell'asta: ");
		etichettaCategoria = new Label("Categoria: ");
		etichettaDomandeRisposte = new Label("Domande e risposte relative all'oggetto: ");
		campoVenditore = new Label();
		campoVenditore.setStyleName("campoVenditore");
		etichettaVenditore = new Label("Utente venditore: ");
		campoMaxOffertaAttuale = new Label();
		campoMaxOffertaAttuale.setStyleName("campoMaxOfferta");
		etichettaMaxOffertaAttuale = new Label("Massima offerta attuale: ");

		informationPanel.setWidget(0, 0, etichettaNomeOggetto);
		informationPanel.setWidget(0, 1, campoNomeOggetto);
		informationPanel.setWidget(1, 0, etichettaVenditore);
		informationPanel.setWidget(1, 1, campoVenditore);
		informationPanel.setWidget(2, 0, etichettaCategoria);
		informationPanel.setWidget(2, 1, campoCategoria);
		informationPanel.setWidget(3, 0, etichettaPrezzoVendita);
		informationPanel.setWidget(3, 1, campoPrezzoVendita);
		informationPanel.setWidget(4, 0, etichettaMaxOffertaAttuale);
		informationPanel.setWidget(4, 1, campoMaxOffertaAttuale);
		informationPanel.setWidget(5, 0, etichettaUtenteVincitore);
		informationPanel.setWidget(5, 1, campoUtenteVincitore);
		informationPanel.setWidget(6, 0, etichettaDataScadenza);
		informationPanel.setWidget(6, 1, campoDataScadenza);
		informationPanel.setWidget(7, 0, etichettaDescrizioneOggetto);
		informationPanel.setWidget(7, 1, campoDescrizioneOggetto);
		informationPanel.setWidget(8, 0, etichettaDomandeRisposte);
		informationPanel.setWidget(8, 1, domandeRisposte);
		informationPanel.setWidget(9, 1, pager);

		if (Sessione.getSessioneUtente() instanceof UtenteAcquirente) {
			informationPanel.setWidget(0, 4, offertaOggetto);
			informationPanel.setWidget(2, 4, domandaOggetto);
		}

		if (Sessione.getSessioneUtente() instanceof UtenteVenditore) {
			informationPanel.setWidget(0, 4, rispostaOggetto);
		}

		rootPanel.add(informationPanel);
		rootPanel.setStyleName("center");
	}

	@Override
	public Label getEtichettaMaxOffertaAttuale() {
		return etichettaMaxOffertaAttuale;
	}

	@Override
	public Label getCampoUtenteVincitore() {
		return campoUtenteVincitore;
	}

	@Override
	public VerticalPanel getDomandaOggetto() {
		return domandaOggetto;
	}

	@Override
	public VerticalPanel getOffertaOggetto() {
		return offertaOggetto;
	}

	@Override
	public Label getCampoNomeOggetto() {
		return campoNomeOggetto;
	}

	@Override
	public Label getCampoDescrizioneOggetto() {
		return campoDescrizioneOggetto;
	}

	@Override
	public Label getCampoPrezzoVendita() {
		return campoPrezzoVendita;
	}

	@Override
	public Label getCampoDataScadenza() {
		return campoDataScadenza;
	}

	@Override
	public Label getCampoMaxOffertaAttuale() {
		return campoMaxOffertaAttuale;
	}

	@Override
	public Label getCampoVenditore() {
		return campoVenditore;
	}

	@Override
	public Label getCampoCategoria() {
		return campoCategoria;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public CellList<String> getListaDomandeRisposte() {
		return domandeRisposte;
	}

	@Override
	public SimplePager getPager() {
		return pager;
	}

	@Override
	public VerticalPanel getRispostaOggetto() {
		return rispostaOggetto;
	}

	@Override
	public Label getEtichettaUtenteVincitore() {
		return etichettaUtenteVincitore;
	}

}
