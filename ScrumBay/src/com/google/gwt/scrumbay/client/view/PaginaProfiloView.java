package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.PaginaProfiloPresenter;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class PaginaProfiloView implements PaginaProfiloPresenter.Display {

	private Label usernameUtente;
	private Label nomeUtente;
	private Label cognomeUtente;
	private Label sessoUtente;
	private Label dataDiNascitaUtente;
	private Label luogoDiNascitaUtente;
	private Label codiceFiscaleUtente;
	private Label telefonoUtente;
	private Label emailUtente;
	private Label domicilioUtente;
	private Label etichettaUsername;
	private Label etichettaNome;
	private Label etichettaCognome;
	private Label etichettaGenere;
	private Label etichettaDataNascita;
	private Label etichettaLuogoNascita;
	private Label etichettaCF;
	private Label etichettaTelefono;
	private Label etichettaEmail;
	private Label etichettaDomicilio;
	private FlexTable mySaleObjects;
	private FlexTable onOfferObjects;
	private Grid userInforPanel;
	private TabPanel informationPanel;
	private VerticalPanel rootPanel;

	public PaginaProfiloView() {
		rootPanel = new VerticalPanel();
		userInforPanel = new Grid(11, 2);
		usernameUtente = new Label();
		usernameUtente.setStyleName("labelLayout");
		nomeUtente = new Label();
		nomeUtente.setStyleName("labelLayout");
		cognomeUtente = new Label();
		cognomeUtente.setStyleName("labelLayout");
		sessoUtente = new Label();
		sessoUtente.setStyleName("labelLayout");
		dataDiNascitaUtente = new Label();
		dataDiNascitaUtente.setStyleName("labelLayout");
		luogoDiNascitaUtente = new Label();
		luogoDiNascitaUtente.setStyleName("labelLayout");
		codiceFiscaleUtente = new Label();
		codiceFiscaleUtente.setStyleName("labelLayout");
		telefonoUtente = new Label();
		telefonoUtente.setStyleName("labelLayout");
		emailUtente = new Label();
		emailUtente.setStyleName("labelLayout");
		domicilioUtente = new Label();
		domicilioUtente.setStyleName("labelLayout");
		etichettaUsername = new Label("Username: ");
		etichettaNome = new Label("Nome: ");
		etichettaCognome = new Label("Cognome: ");
		etichettaGenere = new Label("Sesso: ");
		etichettaDataNascita = new Label("Data di nascita: ");
		etichettaLuogoNascita = new Label("Luogo di nascita: ");
		etichettaCF = new Label("Codice fiscale: ");
		etichettaTelefono = new Label("Telefono: ");
		etichettaEmail = new Label("Email: ");
		etichettaDomicilio = new Label("Domicilio: ");
		mySaleObjects = new FlexTable();
		onOfferObjects = new FlexTable();
		informationPanel = new TabPanel();

		userInforPanel.setWidget(0, 0, etichettaUsername);
		userInforPanel.setWidget(0, 1, usernameUtente);
		userInforPanel.setWidget(1, 0, etichettaNome);
		userInforPanel.setWidget(1, 1, nomeUtente);
		userInforPanel.setWidget(2, 0, etichettaCognome);
		userInforPanel.setWidget(2, 1, cognomeUtente);
		userInforPanel.setWidget(3, 0, etichettaGenere);
		userInforPanel.setWidget(3, 1, sessoUtente);
		userInforPanel.setWidget(4, 0, etichettaDataNascita);
		userInforPanel.setWidget(4, 1, dataDiNascitaUtente);
		userInforPanel.setWidget(5, 0, etichettaLuogoNascita);
		userInforPanel.setWidget(5, 1, luogoDiNascitaUtente);
		userInforPanel.setWidget(6, 0, etichettaCF);
		userInforPanel.setWidget(6, 1, codiceFiscaleUtente);
		userInforPanel.setWidget(7, 0, etichettaTelefono);
		userInforPanel.setWidget(7, 1, telefonoUtente);
		userInforPanel.setWidget(8, 0, etichettaEmail);
		userInforPanel.setWidget(8, 1, emailUtente);
		userInforPanel.setWidget(9, 0, etichettaDomicilio);
		userInforPanel.setWidget(9, 1, domicilioUtente);
		// User tab
		informationPanel.add(userInforPanel, "I miei dati");
		// My on sale objects tab
		informationPanel.add(mySaleObjects, "I miei oggetti in vendita");
		// My offers
		informationPanel.add(onOfferObjects, "Le mie offerte");
		informationPanel.setStyleName("informationPanel");
		informationPanel.selectTab(0);

		rootPanel.add(informationPanel);
	}

	@Override
	public Label getUsername() {
		return usernameUtente;
	}

	@Override
	public Label getNome() {
		return nomeUtente;
	}

	@Override
	public Label getCognome() {
		return cognomeUtente;
	}

	@Override
	public Label getSesso() {
		return sessoUtente;
	}

	@Override
	public Label getDataNascita() {
		return dataDiNascitaUtente;
	}

	@Override
	public Label getLuogoNascita() {
		return luogoDiNascitaUtente;
	}

	@Override
	public Label getCodiFiscale() {
		return codiceFiscaleUtente;
	}

	@Override
	public Label getTelefono() {
		return telefonoUtente;
	}

	@Override
	public Label getEmail() {
		return emailUtente;
	}

	@Override
	public Label getDomicilio() {
		return domicilioUtente;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public FlexTable myOnSaleObjects() {
		return mySaleObjects;
	}

	@Override
	public FlexTable objectsIBidOn() {
		return onOfferObjects;
	}

	@Override
	public TabPanel getInformationPanel() {
		return informationPanel;
	}

}
