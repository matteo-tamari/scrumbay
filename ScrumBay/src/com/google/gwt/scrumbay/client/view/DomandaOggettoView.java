package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.DomandaOggettoPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DomandaOggettoView implements DomandaOggettoPresenter.Display {

	private VerticalPanel rootPanel;
	private TextBox campoDomandaOggetto;
	private Button confirmButton;
	private Label etichettaDomandaOggetto;
	private Label errorLabel;

	public DomandaOggettoView() {
		rootPanel = new VerticalPanel();
		campoDomandaOggetto = new TextBox();
		campoDomandaOggetto.setStyleName("form-control");
		confirmButton = new Button("Chiedi");
		confirmButton.setStyleName("btn btn-primary buttonSpace");
		confirmButton.setEnabled(false);
		etichettaDomandaOggetto = new Label("Inserisci la tua domanda: ");
		errorLabel = new Label("Hai dei dubbi? Chiedi ciò che vuoi al venditore!");

		rootPanel.add(etichettaDomandaOggetto);
		rootPanel.add(campoDomandaOggetto);
		rootPanel.add(confirmButton);
		rootPanel.add(errorLabel);
	}

	@Override
	public TextBox getCampoDomandaOggetto() {
		return campoDomandaOggetto;
	}

	public VerticalPanel getRootPanel() {
		return rootPanel;
	}

	@Override
	public Button getConfirmButton() {
		return confirmButton;
	}

	public Label getEtichettaDomandaOggetto() {
		return etichettaDomandaOggetto;
	}

	@Override
	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}
}
