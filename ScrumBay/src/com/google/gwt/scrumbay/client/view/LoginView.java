package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.LoginPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class LoginView implements LoginPresenter.Display {

	private Grid formPanel;
	private TextBox campoUsername;
	private PasswordTextBox campoPassword;
	private Button confirmButton;
	private Label etichettaUsername;
	private Label etichettaPassword;
	private Label errorLabel;
	private VerticalPanel rootPanel;

	public LoginView() {
		formPanel = new Grid(3, 2);
		rootPanel = new VerticalPanel();

		campoUsername = new TextBox();
		campoUsername.setStyleName("form-control");
		campoPassword = new PasswordTextBox();
		campoPassword.setStyleName("form-control");
		confirmButton = new Button("Conferma");
		confirmButton.setStyleName("btn btn-primary");
		confirmButton.setEnabled(false);
		etichettaUsername = new Label("Username: ");
		etichettaPassword = new Label("Password: ");
		errorLabel = new Label("Inserisci i dati per continuare");

		formPanel.setWidget(0, 0, etichettaUsername);
		formPanel.setWidget(0, 1, campoUsername);
		formPanel.setWidget(1, 0, etichettaPassword);
		formPanel.setWidget(1, 1, campoPassword);
		formPanel.setWidget(2, 1, confirmButton);

		campoUsername.setFocus(true);

		rootPanel.add(errorLabel);
		rootPanel.add(formPanel);
		rootPanel.setStyleName("center");

	}

	@Override
	public TextBox getCampoUsername() {
		return campoUsername;
	}

	@Override
	public PasswordTextBox getCampoPassword() {
		return campoPassword;
	}

	@Override
	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Button getConfirmButton() {
		return confirmButton;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}
}
