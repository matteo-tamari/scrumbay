package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.RegistrationPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class RegistrationView implements RegistrationPresenter.Display {

	private Grid formPanel;
	private HorizontalPanel sexPanel;
	private VerticalPanel rootPanel;
	private TextBox campoUsername;
	private TextBox campoNome;
	private TextBox campoCognome;
	private RadioButton campoGenereM;
	private RadioButton campoGenereF;
	private RadioButton campoGenereA;
	private TextBox campoDataNascita;
	private TextBox campoLuogoNascita;
	private TextBox campoCF;
	private TextBox campoTelefono;
	private TextBox campoEmail;
	private TextBox campoDomicilio;
	private PasswordTextBox campoPassword;
	private PasswordTextBox campoVerificaPassword;
	private Button confirmButton;
	private Label etichettaUsername;
	private Label etichettaNome;
	private Label etichettaCognome;
	private Label etichettaGenere;
	private Label etichettaDataNascita;
	private Label etichettaLuogoNascita;
	private Label etichettaCF;
	private Label etichettaTelefono;
	private Label etichettaEmail;
	private Label etichettaDomicilio;
	private Label etichettaPassword;
	private Label etichettaVerificaPassword;
	private Label errorLabel;

	public RegistrationView() {

		formPanel = new Grid(13, 2);
		sexPanel = new HorizontalPanel();
		rootPanel = new VerticalPanel();

		campoUsername = new TextBox();
		campoUsername.setStyleName("form-control");
		campoNome = new TextBox();
		campoNome.setStyleName("form-control");
		campoCognome = new TextBox();
		campoCognome.setStyleName("form-control");
		campoGenereM = new RadioButton("sesso", "Uomo");
		campoGenereF = new RadioButton("sesso", "Donna");
		campoGenereA = new RadioButton("sesso", "Altro");
		campoDataNascita = new TextBox();
		campoDataNascita.setStyleName("form-control");
		campoLuogoNascita = new TextBox();
		campoLuogoNascita.setStyleName("form-control");
		campoCF = new TextBox();
		campoCF.setStyleName("form-control");
		campoTelefono = new TextBox();
		campoTelefono.setStyleName("form-control");
		campoEmail = new TextBox();
		campoEmail.setStyleName("form-control");
		campoDomicilio = new TextBox();
		campoDomicilio.setStyleName("form-control");
		campoPassword = new PasswordTextBox();
		campoPassword.setStyleName("form-control");
		campoVerificaPassword = new PasswordTextBox();
		campoVerificaPassword.setStyleName("form-control");
		confirmButton = new Button("Conferma");
		confirmButton.setStyleName("btn btn-primary");
		confirmButton.setEnabled(false);
		etichettaUsername = new Label("Username*: ");
		etichettaNome = new Label("Nome*: ");
		etichettaCognome = new Label("Cognome*: ");
		etichettaGenere = new Label("Genere: ");
		etichettaDataNascita = new Label("Data di nascita: ");
		etichettaLuogoNascita = new Label("Luogo di nascita: ");
		etichettaCF = new Label("Codice fiscale*: ");
		etichettaTelefono = new Label("Telefono*: ");
		etichettaEmail = new Label("Email*: ");
		etichettaDomicilio = new Label("Domicilio*: ");
		etichettaPassword = new Label("Password*: ");
		etichettaVerificaPassword = new Label("Verifica password*: ");
		errorLabel = new Label("Inserisci i dati per continuare");

		sexPanel.add(campoGenereM);
		sexPanel.add(campoGenereF);
		sexPanel.add(campoGenereA);

		formPanel.setWidget(0, 0, etichettaUsername);
		formPanel.setWidget(0, 1, campoUsername);
		formPanel.setWidget(1, 0, etichettaNome);
		formPanel.setWidget(1, 1, campoNome);
		formPanel.setWidget(2, 0, etichettaCognome);
		formPanel.setWidget(2, 1, campoCognome);
		formPanel.setWidget(3, 0, etichettaGenere);
		formPanel.setWidget(3, 1, sexPanel);
		formPanel.setWidget(4, 0, etichettaDataNascita);
		formPanel.setWidget(4, 1, campoDataNascita);
		formPanel.setWidget(5, 0, etichettaLuogoNascita);
		formPanel.setWidget(5, 1, campoLuogoNascita);
		formPanel.setWidget(6, 0, etichettaCF);
		formPanel.setWidget(6, 1, campoCF);
		formPanel.setWidget(7, 0, etichettaTelefono);
		formPanel.setWidget(7, 1, campoTelefono);
		formPanel.setWidget(8, 0, etichettaEmail);
		formPanel.setWidget(8, 1, campoEmail);
		formPanel.setWidget(9, 0, etichettaDomicilio);
		formPanel.setWidget(9, 1, campoDomicilio);
		formPanel.setWidget(10, 0, etichettaPassword);
		formPanel.setWidget(10, 1, campoPassword);
		formPanel.setWidget(11, 0, etichettaVerificaPassword);
		formPanel.setWidget(11, 1, campoVerificaPassword);
		formPanel.setWidget(12, 1, confirmButton);

		campoUsername.setFocus(true);

		rootPanel.add(errorLabel);
		rootPanel.add(formPanel);
		rootPanel.setStyleName("center");
	}

	@Override
	public String getSex() {
		if (campoGenereM.getValue())
			return "Uomo";

		if (campoGenereF.getValue())
			return "Donna";

		if (campoGenereA.getValue())
			return "Altro";

		return "";
	}

	@Override
	public TextBox getCampoUsername() {
		return campoUsername;
	}

	@Override
	public TextBox getCampoNome() {
		return campoNome;
	}

	@Override
	public TextBox getCampoCognome() {
		return campoCognome;
	}

	@Override
	public TextBox getCampoDataNascita() {
		return campoDataNascita;
	}

	@Override
	public TextBox getCampoLuogoNascita() {
		return campoLuogoNascita;
	}

	@Override
	public TextBox getCampoCF() {
		return campoCF;
	}

	@Override
	public TextBox getCampoTelefono() {
		return campoTelefono;
	}

	@Override
	public TextBox getCampoEmail() {
		return campoEmail;
	}

	@Override
	public TextBox getCampoDomicilio() {
		return campoDomicilio;
	}

	@Override
	public PasswordTextBox getCampoPassword() {
		return campoPassword;
	}

	@Override
	public PasswordTextBox getCampoVerificaPassword() {
		return campoVerificaPassword;
	}

	@Override
	public Button getConfirmButton() {
		return confirmButton;
	}

	@Override
	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}
}
