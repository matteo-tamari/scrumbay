package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.RenameCategoryPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class RenameCategoryView implements RenameCategoryPresenter.Display {
	private Button bottoneConferma;
	private ListBox elencoCategorie;
	private TextBox nuovoNomeCategoria;
	private Label labelElencoCategorie;
	private Label labelNuovoNomeCategoria;
	private Label sectionLable;

	private Grid formPanel;
	private VerticalPanel rootPanel;

	public RenameCategoryView() {
		formPanel = new Grid(3, 2);
		rootPanel = new VerticalPanel();

		labelElencoCategorie = new Label();
		labelElencoCategorie.setText("Categoria di appartenenza: ");
		elencoCategorie = new ListBox();
		elencoCategorie.setStyleName("custom-select col-md-auto");
		labelNuovoNomeCategoria = new Label();
		labelNuovoNomeCategoria.setText("Nuovo nome categoria: ");
		nuovoNomeCategoria = new TextBox();
		bottoneConferma = new Button("Conferma");
		bottoneConferma.setEnabled(false);
		bottoneConferma.setStyleName("btn btn-primary");

		formPanel.setWidget(0, 0, labelElencoCategorie);
		formPanel.setWidget(0, 1, elencoCategorie);
		formPanel.setWidget(1, 0, labelNuovoNomeCategoria);
		formPanel.setWidget(1, 1, nuovoNomeCategoria);
		formPanel.setWidget(2, 1, bottoneConferma);

		nuovoNomeCategoria.setFocus(true);

		sectionLable = new Label("Form inserimento nuovo nome categoria");

		rootPanel.add(sectionLable);
		rootPanel.add(formPanel);
		rootPanel.setStyleName("center");
	}

	@Override
	public TextBox getCampoNuovoNomeCategoria() {
		return nuovoNomeCategoria;
	}

	@Override
	public ListBox getCampoElencoCategorie() {
		return elencoCategorie;
	}

	@Override
	public Button getBottoneConferma() {
		return bottoneConferma;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public Label getErrorLabel() {
		return sectionLable;
	}
}
