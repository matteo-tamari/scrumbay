package com.google.gwt.scrumbay.client.view;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.LinkedList;

import com.google.gwt.scrumbay.client.presenter.RimozioneOffertaOggettoPresenter;

public class RimozioneOffertaOggettoView implements RimozioneOffertaOggettoPresenter.Display {

	private VerticalPanel rootPanel;
	private Button deleteItemButton;
	private Button deleteOfferButton;
	private FlexTable objectPanel;
	private LinkedList<RadioButton> radioGroup;

	public RimozioneOffertaOggettoView() {
		rootPanel = new VerticalPanel();
		deleteItemButton = new Button("Rimuovi oggetto");
		deleteOfferButton = new Button("Rimuovi offerta");
		rootPanel.setStyleName("center");
		deleteItemButton.setStyleName("btn btn-outline-danger border-danger mx-1");
		deleteOfferButton.setStyleName("btn btn-outline-warning border-warning mx-1");
		objectPanel = new FlexTable();
		radioGroup = new LinkedList<RadioButton>();
		rootPanel.add(objectPanel);
	}

	@Override
	public Button getDeleteOfferButton() {
		return deleteOfferButton;
	}

	@Override
	public VerticalPanel getRootPanel() {
		return rootPanel;
	}

	@Override
	public Button getDeleteItemButton() {
		return deleteItemButton;
	}

	@Override
	public LinkedList<RadioButton> getRadioGroup() {
		return radioGroup;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public FlexTable getObjectPanel() {
		return objectPanel;
	}
}
