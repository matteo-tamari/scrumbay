package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.NavbarPresenter;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.UtenteVisitatore;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class NavbarView implements NavbarPresenter.Display {

	private Button logoButton;
	private Button loginButton;
	private Button logoutButton;
	private Button registrationButton;
	private Button profileButton;
	private Button sellButton;

	private ListBox manageCategoryButton;
	private ListBox manageQuestionAndAnswer;

	private ListBox categoriesListbox;
	private Grid formPanel;
	private HorizontalPanel rootPanel;

	public NavbarView() {
		formPanel = new Grid(1, 6);
		rootPanel = new HorizontalPanel();

		logoButton = new Button();
		loginButton = new Button();
		logoutButton = new Button();
		registrationButton = new Button();
		profileButton = new Button();
		sellButton = new Button();
		categoriesListbox = new ListBox();

		// Utente Amministratore
		manageCategoryButton = new ListBox();
		manageQuestionAndAnswer = new ListBox();

		logoButton.setStyleName("btn border-0 mr-5");
		loginButton.setStyleName("btn btn-outline-success border-success mx-1");
		logoutButton.setStyleName("btn btn-outline-secondary border-secondary mx-1");
		registrationButton.setStyleName("btn btn-outline-danger border-danger mx-1");
		profileButton.setStyleName("btn btn-outline-success border-success mx-1");
		sellButton.setStyleName("btn btn-outline-danger border-danger mx-1");
		categoriesListbox.setStyleName("btn btn-outline-primary border-primary mx-1");

		// Utente Amministratore
		manageCategoryButton.setStyleName("btn btn-outline-warning border-warning mx-1");
		manageQuestionAndAnswer.setStyleName("btn btn-outline-info border-info mx-1 ");

		logoButton.setHTML("<img class=\"mr-5\" src=\"https://i.ibb.co/dmVwZhR/scrumbay.jpg\" alt=\"\">");
		formPanel.setWidget(0, 0, logoButton);

		logoutButton.setText("LOGOUT");

		if (Sessione.getSessioneUtente() instanceof UtenteVisitatore) {
			loginButton.setText("Login");
			registrationButton.setText("Registrati");
			formPanel.setWidget(0, 1, loginButton);
			formPanel.setWidget(0, 2, registrationButton);
		}

		if (Sessione.getSessioneUtente() instanceof UtenteRegistrato) {
			UtenteRegistrato utenteRegistrato = (UtenteRegistrato) Sessione.getSessioneUtente();
			profileButton.setText(utenteRegistrato.getUsername());
			sellButton.setText("Vendi");
			categoriesListbox.setVisibleItemCount(1);
			formPanel.setWidget(0, 1, profileButton);
			formPanel.setWidget(0, 2, sellButton);
			formPanel.setWidget(0, 3, categoriesListbox);
			formPanel.setWidget(0, 5, logoutButton);
		}

		if (Sessione.getSessioneUtente() instanceof UtenteAmministratore) {
			manageCategoryButton.setVisibleItemCount(1);
			manageCategoryButton.addItem("- Gestione categorie -");
			manageCategoryButton.addItem("Nuova categoria");
			manageCategoryButton.addItem("Rinomina categoria");
			manageQuestionAndAnswer.setVisibleItemCount(1);
			manageQuestionAndAnswer.addItem("- Gestione conversazione -");
			manageQuestionAndAnswer.addItem("Elimina domanda");
			manageQuestionAndAnswer.addItem("Elimina risposta");
			formPanel.setWidget(0, 1, manageCategoryButton);
			formPanel.setWidget(0, 2, manageQuestionAndAnswer);
			formPanel.setWidget(0, 5, logoutButton);
		}

		rootPanel.add(formPanel);
	}

	@Override
	public Button getLoginButton() {

		return loginButton;
	}

	@Override
	public Button getLogoutButton() {

		return logoutButton;
	}

	@Override
	public Button getRegistrationButton() {
		return registrationButton;
	}

	@Override
	public Button getProfileButton() {
		return profileButton;
	}

	@Override
	public ListBox getCategoriesListbox() {
		return categoriesListbox;
	}

	@Override
	public Button getSellButton() {
		return sellButton;
	}

	public Button getLogoButton() {
		return logoButton;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public ListBox getManageCategoryButton() {
		return manageCategoryButton;
	}

	@Override
	public ListBox getManageQuestionAndAnswer() {
		return manageQuestionAndAnswer;
	}
}
