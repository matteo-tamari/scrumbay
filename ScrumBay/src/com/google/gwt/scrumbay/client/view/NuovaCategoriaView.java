package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.NuovaCategoriaPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class NuovaCategoriaView implements NuovaCategoriaPresenter.Display {

	// custom-select
	private Button bottoneConferma;
	private ListBox elencoCategorie;
	private TextBox nuovaCategoria;
	private Label labelElencoCategorie;
	private Label labelNuovaCategoria;
	private Label errorLabel;

	private Grid formPanel;
	private VerticalPanel rootPanel;

	public NuovaCategoriaView() {
		formPanel = new Grid(3, 2);
		rootPanel = new VerticalPanel();

		labelElencoCategorie = new Label();
		labelElencoCategorie.setText("Categoria di appartenenza: ");
		elencoCategorie = new ListBox();
		elencoCategorie.setStyleName("custom-select col-md-auto");
		labelNuovaCategoria = new Label();
		labelNuovaCategoria.setText("Nome nuova categoria: ");
		nuovaCategoria = new TextBox();
		bottoneConferma = new Button("Conferma");
		bottoneConferma.setEnabled(false);
		bottoneConferma.setStyleName("btn btn-primary");

		formPanel.setWidget(0, 0, labelElencoCategorie);
		formPanel.setWidget(0, 1, elencoCategorie);
		formPanel.setWidget(1, 0, labelNuovaCategoria);
		formPanel.setWidget(1, 1, nuovaCategoria);
		formPanel.setWidget(2, 1, bottoneConferma);

		nuovaCategoria.setFocus(true);

		errorLabel = new Label("Form inserimento nuova categoria");

		rootPanel.add(errorLabel);
		rootPanel.add(formPanel);
		rootPanel.setStyleName("center");
	}

	@Override
	public TextBox getCampoNuovaCategoria() {
		return nuovaCategoria;
	}

	@Override
	public ListBox getCampoElencoCategorie() {
		return elencoCategorie;
	}

	@Override
	public Button getBottoneConferma() {
		return bottoneConferma;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public Label getErrorLabel() {
		return errorLabel;
	}
}
