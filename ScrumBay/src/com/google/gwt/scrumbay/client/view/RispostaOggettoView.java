package com.google.gwt.scrumbay.client.view;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.scrumbay.client.presenter.RispostaOggettoPresenter;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

public class RispostaOggettoView implements RispostaOggettoPresenter.Display {

	private VerticalPanel rootPanel;
	final SingleSelectionModel<String> domandaSelezionata = new SingleSelectionModel<String>();
	private CellList<String> listaDomande;
	private SimplePager pager;
	private Label domandaLabel;
	private TextBox campoRisposta;
	private Button confirmButton;
	private Label errorLabel;
	private Label etichettaRispostaOggetto;

	public RispostaOggettoView() {
		rootPanel = new VerticalPanel();
		campoRisposta = new TextBox();
		campoRisposta.setStyleName("form-control");
		confirmButton = new Button("Rispondi");
		confirmButton.setStyleName("btn btn-primary buttonSpace");
		confirmButton.setEnabled(false);
		etichettaRispostaOggetto = new Label("Seleziona la domanda a cui rispondere: ");
		etichettaRispostaOggetto.setStyleName("text-info col-md-10 offset-md-1 mt-3");
		errorLabel = new Label("Inserisci una risposta per continuare");
		listaDomande = new CellList<String>(new TextCell());
		listaDomande.setSelectionModel(domandaSelezionata);
		listaDomande.setStyleName("col-md-10 offset-md-1 mt-3");
		pager = new SimplePager();
		pager.setStyleName("col-md-10 offset-md-1 mt-3");
		domandaLabel = new Label();

		rootPanel.add(etichettaRispostaOggetto);
		rootPanel.add(pager);
		rootPanel.add(listaDomande);
		rootPanel.add(domandaLabel);
		rootPanel.add(campoRisposta);
		rootPanel.add(confirmButton);
		rootPanel.add(errorLabel);
	}

	@Override
	public TextBox getCampoRisposta() {
		return campoRisposta;
	}

	public Button getConfirmButton() {
		return confirmButton;
	}

	public Label getEtichettaRispostaOggetto() {
		return etichettaRispostaOggetto;
	}

	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

	@Override
	public SingleSelectionModel<String> getDomandaSelezionata() {
		return domandaSelezionata;
	}

	@Override
	public SimplePager getPager() {
		return pager;
	}

	@Override
	public CellList<String> getListaDomanda() {
		return listaDomande;
	}

	@Override
	public Label getDomandaLabel() {
		return domandaLabel;
	}

}
