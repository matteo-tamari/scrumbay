package com.google.gwt.scrumbay.client.view;

import com.google.gwt.scrumbay.client.presenter.VenditaOggettoPresenter;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class VenditaOggettoView implements VenditaOggettoPresenter.Display {

	private VerticalPanel rootPanel;
	private TextBox campoNomeOggetto;
	private RichTextArea campoDescrizioneOggetto;
	private TextBox campoPrezzoVendita;
	private TextBox campoDataScadenza;
	private ListBox campoCategoria;
	private Button confirmButton;
	private Label etichettaNomeOggetto;
	private Label etichettaDescrizioneOggetto;
	private Label etichettaPrezzoVendita;
	private Label etichettaDataScadenza;
	private Label etichettaCategoria;
	private Label errorLabel;

	public VenditaOggettoView() {
		rootPanel = new VerticalPanel();

		campoNomeOggetto = new TextBox();
		campoNomeOggetto.setStyleName("form-control  col-lg-12");
		campoDescrizioneOggetto = new RichTextArea();
		campoPrezzoVendita = new TextBox();
		campoPrezzoVendita.setStyleName("form-control col-lg-12");
		campoDataScadenza = new TextBox();
		campoDataScadenza.setStyleName("form-control col-lg-12");
		campoCategoria = new ListBox();
		campoCategoria.setStyleName("custom-select mr-sm-2");
		campoCategoria.setVisibleItemCount(1);
		confirmButton = new Button("Conferma");
		confirmButton.setStyleName("btn btn-primary");
		confirmButton.setEnabled(false);
		etichettaNomeOggetto = new Label("Nome dell'oggetto*: ");
		etichettaDescrizioneOggetto = new Label("Descrizione*: ");
		etichettaPrezzoVendita = new Label("Prezzo di partenza*: ");
		etichettaDataScadenza = new Label("Data scadenza dell'asta*: ");
		etichettaCategoria = new Label("Categoria*: ");
		campoDescrizioneOggetto.getFormatter();
		campoDescrizioneOggetto.setStyleName("desciption");
		errorLabel = new Label("Inserisci i dati per continuare");

		rootPanel.add(etichettaNomeOggetto);
		rootPanel.add(campoNomeOggetto);
		rootPanel.add(etichettaPrezzoVendita);
		rootPanel.add(campoPrezzoVendita);
		rootPanel.add(etichettaDataScadenza);
		rootPanel.add(campoDataScadenza);
		rootPanel.add(etichettaCategoria);
		rootPanel.add(campoCategoria);
		rootPanel.add(etichettaDescrizioneOggetto);
		rootPanel.add(campoDescrizioneOggetto);
		rootPanel.add(confirmButton);

		campoNomeOggetto.setMaxLength(50);

		rootPanel.add(errorLabel);

		rootPanel.setStyleName("center big");
	}

	@Override
	public TextBox getCampoNomeOggetto() {
		return campoNomeOggetto;
	}

	@Override
	public RichTextArea getCampoDescrizioneOggetto() {
		return campoDescrizioneOggetto;
	}

	@Override
	public TextBox getCampoPrezzoVendita() {
		return campoPrezzoVendita;
	}

	@Override
	public TextBox getCampoDataScadenza() {
		return campoDataScadenza;
	}

	@Override
	public ListBox getCampoCategoria() {
		return campoCategoria;
	}

	@Override
	public Button getConfirmButton() {
		return confirmButton;
	}

	@Override
	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

}
