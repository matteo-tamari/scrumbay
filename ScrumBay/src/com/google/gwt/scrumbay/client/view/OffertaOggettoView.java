package com.google.gwt.scrumbay.client.view;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.scrumbay.client.presenter.OffertaOggettoPresenter;

public class OffertaOggettoView implements OffertaOggettoPresenter.Display {

	private VerticalPanel rootPanel;
	private TextBox campoOffertaOggetto;
	private Button confirmButton;
	private Label etichettaOffertaOggetto;
	private Label errorLabel;

	public OffertaOggettoView() {
		rootPanel = new VerticalPanel();
		campoOffertaOggetto = new TextBox();
		campoOffertaOggetto.setStyleName("form-control");
		confirmButton = new Button("Conferma");
		confirmButton.setStyleName("btn btn-primary buttonSpace");
		confirmButton.setEnabled(false);
		etichettaOffertaOggetto = new Label("Inserisci la tua offerta: ");
		errorLabel = new Label("Inserisci una offerta per continuare");

		rootPanel.add(etichettaOffertaOggetto);
		rootPanel.add(campoOffertaOggetto);
		rootPanel.add(confirmButton);
		rootPanel.add(errorLabel);
	}

	public VerticalPanel getRootPanel() {
		return rootPanel;
	}

	public TextBox getCampoOffertaOggetto() {
		return campoOffertaOggetto;
	}

	public Button getConfirmButton() {
		return confirmButton;
	}

	public Label getEtichettaOffertaOggetto() {
		return etichettaOffertaOggetto;
	}

	public Label getErrorLabel() {
		return errorLabel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

}
