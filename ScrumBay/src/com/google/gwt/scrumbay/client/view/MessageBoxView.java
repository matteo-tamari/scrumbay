package com.google.gwt.scrumbay.client.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.web.bindery.event.shared.Event;

public class MessageBoxView {

	private final DialogBox dialogBox = new DialogBox();
	private final Button closeButton = new Button("OK");
	private final HTML messageLabel = new HTML();
	private final SimpleEventBus eventBus;
	private final Event<?> eventFired;
	private VerticalPanel dialogVPanel = new VerticalPanel();

	public MessageBoxView() {
		dialogBox.setAnimationEnabled(true);
		closeButton.getElement().setId("okButton");
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(messageLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);
		eventBus = null;
		eventFired = null;
	}

	public MessageBoxView(SimpleEventBus eventBus, Event<?> event) {
		this.eventBus = eventBus;
		this.eventFired = event;
		dialogBox.setAnimationEnabled(true);
		closeButton.getElement().setId("okButton");
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(messageLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);
	}

	public void showMessageBoxView(String title, String message) {
		dialogBox.setText(title);
		messageLabel.setHTML(message);
		dialogBox.center();
		closeButton.setFocus(true);
		bind();
	}

	private void bind() {
		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (eventFired != null) {
					eventBus.fireEvent(eventFired);
				}
				dialogBox.hide();
			}
		});
	}
}
