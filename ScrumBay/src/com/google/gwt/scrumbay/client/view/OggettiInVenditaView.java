package com.google.gwt.scrumbay.client.view;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import java.util.LinkedList;

import com.google.gwt.scrumbay.client.presenter.OggettiInVenditaPresenter;

public class OggettiInVenditaView implements OggettiInVenditaPresenter.Display {

	private VerticalPanel rootPanel;
	private Button viewDetailsObject;
	private FlexTable objectPanel;
	private LinkedList<Hyperlink> objectsLink;

	public OggettiInVenditaView() {
		rootPanel = new VerticalPanel();
		viewDetailsObject = new Button("Visualizza Dettagli");
		viewDetailsObject.setStyleName("btn btn-outline-info border-info mx-1");
		rootPanel.setStyleName("center");
		objectPanel = new FlexTable();
		objectsLink = new LinkedList<Hyperlink>();
		rootPanel.add(objectPanel);
	}

	@Override
	public VerticalPanel getRootPanel() {
		return rootPanel;
	}

	@Override
	public LinkedList<Hyperlink> getObjectsLink() {
		return objectsLink;
	}

	@Override
	public FlexTable getObjectPanel() {
		return objectPanel;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

}
