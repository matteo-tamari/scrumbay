package com.google.gwt.scrumbay.client.view;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.scrumbay.client.presenter.RimozioneDomandaRispostaPresenter;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

public class RimozioneDomandaRispostaView implements RimozioneDomandaRispostaPresenter.Display {

	private Label infoLabel;
	private CellList<String> listaOggetti;
	private ListBox listaDomande;
	private Label labelDomanda;
	private Label labelRisposta;
	private Button eliminaDomanda;
	private Button eliminaRisposta;
	private FlexTable selectionPanel;
	private VerticalPanel rootPanel;
	private SimplePager pager;
	final SingleSelectionModel<String> oggettoSelezionato = new SingleSelectionModel<String>();

	public RimozioneDomandaRispostaView(int typePage) {
		selectionPanel = new FlexTable();
		rootPanel = new VerticalPanel();

		infoLabel = new Label("Seleziona ciò che desideri eliminare");
		infoLabel.setStyleName("text-info col-md-10 offset-md-1 mt-3");
		listaOggetti = new CellList<String>(new TextCell());
		listaOggetti.setSelectionModel(oggettoSelezionato);
		listaOggetti.setStyleName("col-md-10 offset-md-1 mt-3");
		pager = new SimplePager();
		pager.setStyleName("col-md-10 offset-md-1 mt-3");
		listaDomande = new ListBox();
		listaDomande.addItem("- ELENCO DOMANDE OGGETTO -");
		listaDomande.setVisibleItemCount(1);
		listaDomande.setStyleName("border-info custom-select col-md-12 mt-3");
		labelDomanda = new Label("DOMANDA: ");
		labelRisposta = new Label("RISPOSTA: ");
		eliminaDomanda = new Button("ELIMINA DOMANDA");
		eliminaDomanda.setStyleName("btn btn-outline-danger border-danger col-md-8 offset-md-2 mt-3");
		eliminaDomanda.setEnabled(false);
		eliminaRisposta = new Button("ELIMINA RISPOSTA");
		eliminaRisposta.setStyleName("btn btn-outline-warning border-warning col-md-8 offset-md-2 mt-3");
		eliminaRisposta.setEnabled(false);

		selectionPanel.setWidget(0, 0, infoLabel);
		selectionPanel.setWidget(1, 0, pager);
		selectionPanel.setWidget(2, 0, listaOggetti);
		selectionPanel.setWidget(3, 0, listaDomande);
		selectionPanel.setWidget(4, 0, labelDomanda);
		selectionPanel.setWidget(5, 0, labelRisposta);
		if (typePage == 0) {
			selectionPanel.setWidget(6, 0, eliminaDomanda);
			labelDomanda.setStyleName("font-weight-bold text-danger col-md-10 offset-md-1 mt-3");
			labelRisposta.setStyleName("font-weight-bold text-danger col-md-10 offset-md-1");
		}
		if (typePage == 1) {
			selectionPanel.setWidget(6, 0, eliminaRisposta);
			labelDomanda.setStyleName("font-weight-bold text-secondary col-md-10 offset-md-1 mt-3");
			labelRisposta.setStyleName("font-weight-bold text-danger col-md-10 offset-md-1");
		}

		rootPanel.add(selectionPanel);
		rootPanel.setStyleName("center");
	}

	@Override
	public SingleSelectionModel<String> getOggettoSelezionato() {
		return oggettoSelezionato;
	}

	@Override
	public SimplePager getPager() {
		return pager;
	}

	@Override
	public Label getInfoLabel() {
		return infoLabel;
	}

	@Override
	public CellList<String> getListaOggetti() {
		return listaOggetti;
	}

	@Override
	public ListBox getListaDomande() {
		return listaDomande;
	}

	@Override
	public Label getCampoDomanda() {
		return labelDomanda;
	}

	@Override
	public Label getCampoRisposta() {
		return labelRisposta;
	}

	@Override
	public Button getEliminaDomandaButton() {
		return eliminaDomanda;
	}

	@Override
	public Button getEliminaRispostaButton() {
		return eliminaRisposta;
	}

	@Override
	public Widget asWidget() {
		return rootPanel;
	}

}
