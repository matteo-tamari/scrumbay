package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class OfferEvent extends GwtEvent<OfferEventHandler> {

	public static Type<OfferEventHandler> TYPE = new Type<OfferEventHandler>();

	@Override
	public Type<OfferEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OfferEventHandler handler) {
		handler.onNewOffer(this);
	}
}
