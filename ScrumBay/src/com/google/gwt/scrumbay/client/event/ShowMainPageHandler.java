package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowMainPageHandler extends EventHandler {
	void onNewRefresh(ShowMainPageEvent event);
}
