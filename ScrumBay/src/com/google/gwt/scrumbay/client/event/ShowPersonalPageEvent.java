package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ShowPersonalPageEvent extends GwtEvent<ShowPersonalPageHandler> {

	public static Type<ShowPersonalPageHandler> TYPE = new Type<ShowPersonalPageHandler>();

	@Override
	public Type<ShowPersonalPageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ShowPersonalPageHandler handler) {
		handler.onPersonalPageRequest(this);
	}

}
