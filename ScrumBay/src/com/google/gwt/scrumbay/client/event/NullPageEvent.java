package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class NullPageEvent extends GwtEvent<NullPageEventHandler> {

	public static Type<NullPageEventHandler> TYPE = new Type<NullPageEventHandler>();

	@Override
	public Type<NullPageEventHandler> getAssociatedType() {

		return TYPE;
	}

	@Override
	protected void dispatch(NullPageEventHandler handler) {

		handler.onNullRequest(this);
	}

}
