package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NewCategoryHandler extends EventHandler {
	void onNewCategory(NewCategoryEvent event);
}
