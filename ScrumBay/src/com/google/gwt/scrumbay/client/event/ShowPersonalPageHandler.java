package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ShowPersonalPageHandler extends EventHandler {
	void onPersonalPageRequest(ShowPersonalPageEvent event);
}
