package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RenameCategoryHandler extends EventHandler {
	void onRename(RenameCategoryEvent event);
}