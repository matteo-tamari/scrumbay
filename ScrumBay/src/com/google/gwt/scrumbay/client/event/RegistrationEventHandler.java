package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RegistrationEventHandler extends EventHandler {
	void onNewRegistration(RegistrationEvent event);
}
