package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RenameCategoryEvent extends GwtEvent<RenameCategoryHandler> {

	public static Type<RenameCategoryHandler> TYPE = new Type<RenameCategoryHandler>();

	@Override
	public Type<RenameCategoryHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RenameCategoryHandler handler) {
		handler.onRename(this);
	}

}
