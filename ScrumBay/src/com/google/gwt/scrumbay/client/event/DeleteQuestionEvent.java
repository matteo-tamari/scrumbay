package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class DeleteQuestionEvent extends GwtEvent<DeleteQuestionEventHandler> {

	public static Type<DeleteQuestionEventHandler> TYPE = new Type<DeleteQuestionEventHandler>();

	@Override
	public Type<DeleteQuestionEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeleteQuestionEventHandler handler) {
		handler.onDeleteQuestion(this);
	}
}
