package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.scrumbay.shared.Oggetto;

public class ViewDetailsEvent extends GwtEvent<ViewDetailsEventHandler> {

	private Oggetto oggettoDaVisualizzare;

	public ViewDetailsEvent(Oggetto oggettoDaVisualizzare) {
		this.oggettoDaVisualizzare = oggettoDaVisualizzare;
	}

	public static Type<ViewDetailsEventHandler> TYPE = new Type<ViewDetailsEventHandler>();

	@Override
	public Type<ViewDetailsEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ViewDetailsEventHandler handler) {
		handler.onDetailsRequest(this);
	}

	public Oggetto getOggettoDaVisualizzare() {
		return oggettoDaVisualizzare;
	}

}
