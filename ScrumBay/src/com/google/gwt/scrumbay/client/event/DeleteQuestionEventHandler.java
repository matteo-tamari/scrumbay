package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteQuestionEventHandler extends EventHandler {
	void onDeleteQuestion(DeleteQuestionEvent event);
}
