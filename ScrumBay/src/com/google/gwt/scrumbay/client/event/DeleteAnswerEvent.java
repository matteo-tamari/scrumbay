package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class DeleteAnswerEvent extends GwtEvent<DeleteAnswerEventHandler> {

	public static Type<DeleteAnswerEventHandler> TYPE = new Type<DeleteAnswerEventHandler>();

	@Override
	public Type<DeleteAnswerEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeleteAnswerEventHandler handler) {
		handler.onDeleteAnswer(this);
	}
}
