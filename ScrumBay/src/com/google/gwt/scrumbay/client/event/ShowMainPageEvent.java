package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class ShowMainPageEvent extends GwtEvent<ShowMainPageHandler> {

	public static Type<ShowMainPageHandler> TYPE = new Type<ShowMainPageHandler>();
	private String category;

	public ShowMainPageEvent() {
		this.category = "NULL";
	}

	public ShowMainPageEvent(String category) {
		this.category = category;
	}

	@Override
	public Type<ShowMainPageHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ShowMainPageHandler handler) {
		handler.onNewRefresh(this);
	}

	/**
	 * L'evento puo' essere creato dando in input una categoria in questo caso la
	 * main page verra' caricata mostrando gli oggetti ad essa appartenente
	 * 
	 * @return category
	 */
	public String getCategory() {
		return category;
	}

}
