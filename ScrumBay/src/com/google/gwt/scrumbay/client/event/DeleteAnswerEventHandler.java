package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DeleteAnswerEventHandler extends EventHandler {
	void onDeleteAnswer(DeleteAnswerEvent event);
}
