package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface ViewDetailsEventHandler extends EventHandler {
	void onDetailsRequest(ViewDetailsEvent event);
}
