package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.scrumbay.client.event.NullPageEvent;

public interface NullPageEventHandler extends EventHandler {
	public void onNullRequest(NullPageEvent event);
}