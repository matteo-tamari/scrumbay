package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class NewCategoryEvent extends GwtEvent<NewCategoryHandler> {

	public static Type<NewCategoryHandler> TYPE = new Type<NewCategoryHandler>();

	@Override
	public Type<NewCategoryHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NewCategoryHandler handler) {
		handler.onNewCategory(this);
	}

}
