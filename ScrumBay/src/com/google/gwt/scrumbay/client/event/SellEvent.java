package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class SellEvent extends GwtEvent<SellEventHandler> {

	public static Type<SellEventHandler> TYPE = new Type<SellEventHandler>();

	@Override
	public Type<SellEventHandler> getAssociatedType() {

		return TYPE;
	}

	@Override
	protected void dispatch(SellEventHandler handler) {

		handler.onNewSell(this);

	}

}
