package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface OfferEventHandler extends EventHandler {
	void onNewOffer(OfferEvent offer);
}
