package com.google.gwt.scrumbay.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RegistrationEvent extends GwtEvent<RegistrationEventHandler> {

	public static Type<RegistrationEventHandler> TYPE = new Type<RegistrationEventHandler>();

	@Override
	public Type<RegistrationEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RegistrationEventHandler handler) {
		handler.onNewRegistration(this);
	}

}
