package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.NewCategoryEvent;
import com.google.gwt.scrumbay.client.service.CategoryService;
import com.google.gwt.scrumbay.client.service.CategoryServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class NuovaCategoriaPresenter implements Presenter {

	public interface Display {
		public TextBox getCampoNuovaCategoria();

		public ListBox getCampoElencoCategorie();

		public Button getBottoneConferma();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private boolean btnLockCategoriaPadre = false;
	private boolean btnLocknuovaCategoria = false;
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private final Display view;
	private LinkedList<Categoria> elencoCategorie;

	public NuovaCategoriaPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new NewCategoryEvent());
		this.messageBoxOnFailure = new MessageBoxView();
		elencoCategorie = new LinkedList<Categoria>();
	}

	@Override
	public void bind() {
		view.getCampoElencoCategorie().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String categoriaPadre = view.getCampoElencoCategorie().getSelectedItemText();
				if (categoriaPadre.isEmpty()) {
					btnLockCategoriaPadre = false;
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Selezionare una categoria di riferimento.");
				} else {
					btnLockCategoriaPadre = true;
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Categoria di riferimento OK.");
					checkTextBoxes();
					aggiornaElencoCategorie(view.getCampoElencoCategorie().getSelectedValue());
				}
			}
		});

		view.getCampoNuovaCategoria().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoNuovaCategoria().getText();
				if (!input.matches("[a-zA-Z ]{3,20}")) {
					btnLocknuovaCategoria = false;
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Nome nuova categoria non valido.");
				} else {
					btnLocknuovaCategoria = true;
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Nome nuova categoria OK.");
					checkTextBoxes();
				}
			}
		});

		view.getBottoneConferma().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				CategoryServiceAsync nuovaCategoriaSvc = GWT.create(CategoryService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE CREAZIONE NUOVA CATEGORIA:",
								caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("CREAZIONE NUOVA CATEGORIA EFFETTUATA:", result);
					}
				};
				nuovaCategoriaSvc.addNewCategory(view.getCampoElencoCategorie().getSelectedValue(),
						view.getCampoNuovaCategoria().getText(), callback);
			}
		});
	}

	private void checkTextBoxes() {
		if (btnLockCategoriaPadre && btnLocknuovaCategoria) {
			view.getBottoneConferma().setEnabled(true);
		}
	}

	private void aggiornaElencoCategorie(final String categoriaPadre) {
		CategoryServiceAsync categorieSvc = GWT.create(CategoryService.class);
		AsyncCallback<LinkedList<Categoria>> callback = new AsyncCallback<LinkedList<Categoria>>() {
			@Override
			public void onFailure(Throwable caught) {
				view.getErrorLabel().setText(caught.getMessage());
			}

			@Override
			public void onSuccess(LinkedList<Categoria> result) {
				view.getCampoElencoCategorie().clear();
				if (categoriaPadre.equals("NULL")) {
					view.getCampoElencoCategorie().addItem("- ELENCO CATEGORIE -", categoriaPadre);
					elencoCategorie = result;
				} else {
					view.getCampoElencoCategorie().addItem(categoriaPadre);
					if (result.size() > 0) {
						view.getCampoElencoCategorie().addItem("<-- Back",
								result.getLast().getCategoriaPadre().getCategoriaPadre().getNomeCategoria());
						elencoCategorie = result;
					} else {
						view.getCampoElencoCategorie().addItem("<-- Back",
								elencoCategorie.getLast().getCategoriaPadre().getNomeCategoria());
					}
				}

				for (Categoria categoria : result) {
					view.getCampoElencoCategorie().addItem("     " + categoria.getNomeCategoria(),
							categoria.getNomeCategoria());
				}
			}

		};
		categorieSvc.getCategories(categoriaPadre, callback);
	}

	@Override
	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
		aggiornaElencoCategorie("NULL");
	}
}
