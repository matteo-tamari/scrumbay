package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

public interface Presenter {

	public void go(final HasWidgets container);

	public void bind();

}
