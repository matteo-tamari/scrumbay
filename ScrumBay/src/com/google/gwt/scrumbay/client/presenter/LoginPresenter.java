package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.LoginService;
import com.google.gwt.scrumbay.client.service.LoginServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginPresenter implements Presenter {

	public interface Display {
		public TextBox getCampoUsername();

		public PasswordTextBox getCampoPassword();

		public Label getErrorLabel();

		public Button getConfirmButton();

		public Widget asWidget();
	}

	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	final private Display view;

	public LoginPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new ShowMainPageEvent());
		this.messageBoxOnFailure = new MessageBoxView();
	}

	@Override
	public void bind() {
		view.getCampoUsername().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoUsername().getText();
				if (!input.matches("^[a-zA-Z0-9]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo Username.");
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Username OK.");
					checkTextBoxes();
				}
			}
		});
		view.getCampoPassword().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoPassword().getText();
				if (input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Password non valida.");
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Password OK.");
					checkTextBoxes();
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				LoginServiceAsync login = GWT.create(LoginService.class);
				AsyncCallback<Utente> callback = new AsyncCallback<Utente>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE DI LOGIN:", caught.getMessage());
					}

					@Override
					public void onSuccess(Utente result) {
						Sessione.setSessioneUtente(result);
						messageBoxOnSuccess.showMessageBoxView("LOGIN EFFETTUATO:",
								"Autenticazione utente andata a buon fine!");
					}
				};
				login.signIn(view.getCampoUsername().getText(), view.getCampoPassword().getText(), callback);
			}
		});
	}

	private void checkTextBoxes() {
		if (!view.getCampoUsername().getText().isEmpty() && !view.getCampoPassword().getText().isEmpty()) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
	}

}
