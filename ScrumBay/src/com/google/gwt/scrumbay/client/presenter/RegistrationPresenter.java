package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.service.RegistrationService;
import com.google.gwt.scrumbay.client.service.RegistrationServiceAsync;
import com.google.gwt.scrumbay.client.event.LoginEvent;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.client.view.RegistrationView;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Questo presenter contiene la logica relativa alla view della registrazione
 * popola effettua i controlli sui dati passati e invoca la rpc verso il server.
 * Tutti i presenter implementano l'interfaccia Presenter che dichiara il metodo
 * go, questo contiene la logica del presenter.
 */
public class RegistrationPresenter implements Presenter {

	/**
	 * Interfaccia implementata dalla view, mette a disposizione i metodi per
	 * ritornare i widget, cio' e' necessario per configurare gli handler.
	 */
	public interface Display {
		public String getSex();

		public TextBox getCampoUsername();

		public TextBox getCampoNome();

		public TextBox getCampoCognome();

		public TextBox getCampoDataNascita();

		public TextBox getCampoLuogoNascita();

		public TextBox getCampoCF();

		public TextBox getCampoTelefono();

		public TextBox getCampoEmail();

		public TextBox getCampoDomicilio();

		public PasswordTextBox getCampoPassword();

		public PasswordTextBox getCampoVerificaPassword();

		public Button getConfirmButton();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private final Display view;
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private boolean usernameBtnLock = false;
	private boolean nomeBtnLock = false;
	private boolean cognomeBtnLock = false;
	private boolean telefonoBtnLock = false;
	private boolean emailBtnLock = true;
	private boolean passwordBtnLock = false;
	private boolean passwordCheckBtnLock = false;
	private boolean codiceFiscaleBtnLock = false;
	private boolean domicilioBtnLock = false;

	public RegistrationPresenter(RegistrationView view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new LoginEvent());
		this.messageBoxOnFailure = new MessageBoxView();
	}

	public void bind() {
		view.getCampoUsername().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoUsername().getText();
				if (!input.matches("^[a-zA-Z0-9]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo Username.");
					usernameBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Username OK.");
					usernameBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoNome().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoNome().getText();
				if (!input.matches("^[A-Z]{1}[a-z]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La prima lettera deve essere una maiuscola seguita da minuscole.");
					nomeBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Nome OK.");
					nomeBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoCognome().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoCognome().getText();
				if (!input.matches("^[A-Z]{1}[a-z]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La prima lettera deve essere una maiuscola seguita da minuscole.");
					cognomeBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Cognome OK.");
					cognomeBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoLuogoNascita().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoLuogoNascita().getText();
				if (!input.matches("^$|^[A-Za-z,\"  *\"]+$")) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La prima lettera deve essere una maiuscola seguita da minuscole.");
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Luogo nascita OK.");
					checkTextBoxes();
				}
			}
		});

		view.getCampoCF().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoCF().getText();
				if (!input.matches("^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo CF.");
					codiceFiscaleBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Codice fiscale OK.");
					codiceFiscaleBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoTelefono().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoTelefono().getText();
				if (!input.matches("^$|^([+0-9\" \"]{7,15})+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo Telefono.");
					telefonoBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Telefono OK.");
					telefonoBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoDataNascita().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoDataNascita().getText();
				if (!input.matches(
						"^$|^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([1-2]{1}[0|9]{1}[0-9]{1}[0-9]{1})+$")) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La data deve avere il formato gg/mm/aaaa");
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Data di nascita OK.");
					checkTextBoxes();
				}
			}
		});

		view.getCampoEmail().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoEmail().getText();
				if (!input.matches("^[a-z0-9]+@[a-z]+\\.[a-z]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La mail deve avere forma: username@dominio.xxx.");
					emailBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Email OK.");
					emailBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoDomicilio().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoDomicilio().getText();
				if (!input.matches("^[A-Za-z0-9,\"  *\"]+$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo Domicilio.");
					domicilioBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Domicilio OK.");
					domicilioBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoPassword().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoPassword().getText();
				if (!input.matches("[a-zA-Z0-9!@#$%^&*?]{8,}") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText(
							"La password deve contenere tra gli 8 e i 40 caratteri. Non sono ammesse parentesi.");
					passwordBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Password OK.");
					passwordBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoVerificaPassword().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoVerificaPassword().getText();
				if (!input.matches(view.getCampoPassword().getText()) || !passwordBtnLock) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Errore nel campo Verifica passowrd.");
					passwordCheckBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Verifica OK.");
					passwordCheckBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String username = view.getCampoUsername().getText();
				String nome = view.getCampoNome().getText();
				String cognome = view.getCampoCognome().getText();
				String telefono = view.getCampoTelefono().getText();
				String password = view.getCampoPassword().getText();
				String email = view.getCampoEmail().getText();
				String codiceFiscale = view.getCampoCF().getText();
				String domicilio = view.getCampoDomicilio().getText();
				String sesso = view.getSex();
				String dataNascita = view.getCampoDataNascita().getText();
				String luogoNascita = view.getCampoLuogoNascita().getText();

				RegistrationServiceAsync registrazioneUtenteSvc = GWT.create(RegistrationService.class);

				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE DI REGISTRAZIONE", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("REGISTRAZIONE EFFETTUATA", result);
					}
				};
				registrazioneUtenteSvc.addRegistration(username, nome, cognome, telefono, password, email,
						codiceFiscale, domicilio, sesso, dataNascita, luogoNascita, callback);
			}
		});
	}

	private void checkTextBoxes() {
		if (usernameBtnLock && nomeBtnLock && cognomeBtnLock && telefonoBtnLock && emailBtnLock && passwordBtnLock
				&& passwordCheckBtnLock && codiceFiscaleBtnLock && domicilioBtnLock) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
	}
}
