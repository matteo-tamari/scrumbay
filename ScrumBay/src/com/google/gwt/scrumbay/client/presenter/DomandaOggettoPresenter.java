package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.QuestionAnswerService;
import com.google.gwt.scrumbay.client.service.QuestionAnswerServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class DomandaOggettoPresenter implements Presenter {

	public interface Display {
		public TextBox getCampoDomandaOggetto();

		public Button getConfirmButton();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private final Display view;
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private boolean domandaOggettoBtnLock = false;
	private final Oggetto oggetto;

	public DomandaOggettoPresenter(Display view, SimpleEventBus eventBus, Oggetto oggetto) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new ShowMainPageEvent());
		this.messageBoxOnFailure = new MessageBoxView();
		this.oggetto = oggetto;
	}

	public void bind() {
		view.getCampoDomandaOggetto().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (view.getCampoDomandaOggetto().getValue().isEmpty()
						|| view.getCampoDomandaOggetto().getText().length() <= 5) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Il campo non puo' essere vuoto ");
					domandaOggettoBtnLock = false;
					checkConfirmButtonLock();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Campo inserito correttamente");
					domandaOggettoBtnLock = true;
					checkConfirmButtonLock();
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QuestionAnswerServiceAsync domandaSvc = GWT.create(QuestionAnswerService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE INSERIMENTO DOMANDA: ", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("DOMANDA INSERITA CORRETTAMENTE: ", result);
					}
				};
				Domanda domanda = new Domanda(view.getCampoDomandaOggetto().getText(),
						(UtenteAcquirente) Sessione.getSessioneUtente());
				domandaSvc.newQuestion(oggetto.getKey(), domanda, callback);
			}
		});
	}

	public void checkConfirmButtonLock() {
		if (domandaOggettoBtnLock) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

	@Override
	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
	}

}
