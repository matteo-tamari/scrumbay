package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.view.DomandaOggettoView;
import com.google.gwt.scrumbay.client.view.OffertaOggettoView;
import com.google.gwt.scrumbay.client.view.RispostaOggettoView;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.StatoVendita;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent;

public class InformazioniOggettoPresenter implements Presenter {

	public interface Display {

		public Label getCampoNomeOggetto();

		public Label getCampoDescrizioneOggetto();

		public Label getCampoPrezzoVendita();

		public Label getCampoDataScadenza();

		public Label getCampoCategoria();

		public Label getCampoMaxOffertaAttuale();

		public Label getCampoVenditore();

		public Label getCampoUtenteVincitore();

		public Label getEtichettaUtenteVincitore();

		public Label getEtichettaMaxOffertaAttuale();

		public VerticalPanel getOffertaOggetto();

		public VerticalPanel getDomandaOggetto();

		public CellList<String> getListaDomandeRisposte();

		public SimplePager getPager();

		public VerticalPanel getRispostaOggetto();

		public Widget asWidget();
	}

	private final Display view;
	private final Oggetto oggetto;
	private final OffertaOggettoPresenter presenterOfferta;
	private final OffertaOggettoView visualizzazioneOfferta;
	private final DomandaOggettoPresenter presenterDomanda;
	private final DomandaOggettoView visualizzazioneDomanda;
	private LinkedList<String> domandeRisposte;

	private final RispostaOggettoPresenter presenterRisposta;
	private final RispostaOggettoView visualizzazioneRisposta;

	public InformazioniOggettoPresenter(Display view, SimpleEventBus eventBus, Oggetto oggetto) {
		this.view = view;
		this.oggetto = oggetto;
		this.visualizzazioneOfferta = new OffertaOggettoView();
		this.presenterOfferta = new OffertaOggettoPresenter(visualizzazioneOfferta, eventBus, oggetto);
		this.visualizzazioneDomanda = new DomandaOggettoView();
		this.presenterDomanda = new DomandaOggettoPresenter(visualizzazioneDomanda, eventBus, oggetto);
		this.visualizzazioneRisposta = new RispostaOggettoView();
		this.presenterRisposta = new RispostaOggettoPresenter(visualizzazioneRisposta, eventBus, oggetto);
	}

	private void getListaDomandeRisposte() {
		domandeRisposte = new LinkedList<String>();
		for (int i = 0; i < oggetto.getDomande().size(); i++) {
			domandeRisposte.add("DOMANDA: " + oggetto.getDomande().get(i).getDomanda());
			if (oggetto.getDomande().get(i).getReplica() == null) {
				domandeRisposte.add("RISPOSTA: non fornita!");
			} else {
				domandeRisposte.add("RISPOSTA: " + oggetto.getDomande().get(i).getReplica().getRisposta());
			}

		}
		view.getListaDomandeRisposte().setRowData(0, domandeRisposte);
		view.getListaDomandeRisposte().setVisibleRangeAndClearData(new Range(0, 2), true);
		view.getPager().setDisplay(view.getListaDomandeRisposte());

	}

	public void fillFields() {
		view.getCampoNomeOggetto().setText(oggetto.getNome());
		view.getCampoDescrizioneOggetto().setText(oggetto.getDescrizione());
		view.getCampoPrezzoVendita().setText(new Double(oggetto.getPrezzoBase()).toString());
		view.getCampoDataScadenza().setText(oggetto.getDataScadenza());
		view.getCampoCategoria().setText(oggetto.getCategoria().getNomeCategoria());
		view.getCampoVenditore().setText(oggetto.getVenditore().getUsername());
		view.getCampoMaxOffertaAttuale().setText(new Double(oggetto.getMiglioreOfferta().getImporto()).toString());
		view.getDomandaOggetto().add(visualizzazioneDomanda.asWidget());
		view.getOffertaOggetto().add(visualizzazioneOfferta.asWidget());
		view.getRispostaOggetto().add(visualizzazioneRisposta.asWidget());
		if (oggetto.getStato().contains(StatoVendita.ASTACONCLUSA.getStato())) {
			view.getCampoUtenteVincitore().setText(oggetto.getOfferte().getLast().getAcquirente().getUsername());
			view.getEtichettaMaxOffertaAttuale().setText("Offerta vincente: ");
		} else {
			view.getCampoUtenteVincitore().setVisible(false);
			view.getEtichettaUtenteVincitore().setVisible(false);
		}
	}

	@Override
	public void bind() {
		this.presenterOfferta.go(view.getOffertaOggetto());
		this.presenterDomanda.go(view.getDomandaOggetto());
		this.presenterRisposta.go(view.getRispostaOggetto());

		view.getListaDomandeRisposte().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				Range range = event.getNewRange();
				int start = range.getStart();
				int length = range.getLength();

				if (start >= domandeRisposte.size()) {
					view.getPager().setPage(0);
				} else {

					if (start + length > domandeRisposte.size() && domandeRisposte.size() > start) {
						length = domandeRisposte.size() - start;
					}

					LinkedList<String> data = new LinkedList<String>();
					for (int i = start; i < start + length; i++) {
						data.add(domandeRisposte.get(i));
					}

					view.getListaDomandeRisposte().setRowData(start, data);
				}
			}
		});

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		fillFields();
		bind();
		getListaDomandeRisposte();

	}

}
