package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.OnSaleService;
import com.google.gwt.scrumbay.client.service.OnSaleServiceAsync;
import com.google.gwt.scrumbay.client.service.QuestionAnswerService;
import com.google.gwt.scrumbay.client.service.QuestionAnswerServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class RimozioneDomandaRispostaPresenter implements Presenter {

	public interface Display {
		public Label getInfoLabel();

		public CellList<String> getListaOggetti();

		public ListBox getListaDomande();

		public Label getCampoDomanda();

		public Label getCampoRisposta();

		public Button getEliminaDomandaButton();

		public Button getEliminaRispostaButton();

		public SingleSelectionModel<String> getOggettoSelezionato();

		public SimplePager getPager();

		public Widget asWidget();
	}

	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private LinkedList<String> oggettiKey;
	private LinkedList<Oggetto> oggetti;
	final private Display view;
	private boolean lockBottone = true;

	public RimozioneDomandaRispostaPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new ShowMainPageEvent());
		this.messageBoxOnFailure = new MessageBoxView();
	}

	private void getListaOggetti() {
		OnSaleServiceAsync getOnSaleObject = GWT.create(OnSaleService.class);
		AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				messageBoxOnFailure.showMessageBoxView("ERRORE NEL RECUPERO DEGLI OGGETTI IN VENDITA",
						caught.getMessage());
			}

			@Override
			public void onSuccess(LinkedList<Oggetto> result) {
				oggetti = result;
				oggettiKey = new LinkedList<String>();
				for (int i = 0; i < result.size(); i++) {
					oggettiKey.add(result.get(i).getKey());
				}
				view.getListaOggetti().setRowData(0, oggettiKey);
				view.getListaOggetti().setVisibleRangeAndClearData(new Range(0, 5), true);
				view.getPager().setDisplay(view.getListaOggetti());
			}
		};
		getOnSaleObject.getOnSaleObjects(callback);
	}

	@Override
	public void bind() {

		view.getListaOggetti().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				Range range = event.getNewRange();
				int start = range.getStart();
				int length = range.getLength();

				if (start >= oggettiKey.size()) {
					view.getPager().setPage(0);
				} else {

					if (start + length > oggettiKey.size() && oggettiKey.size() > start) {
						length = oggettiKey.size() - start;
					}

					LinkedList<String> data = new LinkedList<String>();
					for (int i = start; i < start + length; i++) {
						data.add(oggettiKey.get(i));
					}

					view.getListaOggetti().setRowData(start, data);
				}
			}
		});

		view.getOggettoSelezionato().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				lockBottone = true;
				String selected = view.getOggettoSelezionato().getSelectedObject();
				if (selected != null) {
					for (Oggetto oggetto : oggetti) {
						if (oggetto.getKey().equals(selected)) {
							view.getListaDomande().clear();
							view.getListaDomande().addItem("- ELENCO DOMANDE OGGETTO -");
							for (Domanda domanda : oggetto.getDomande()) {
								if (lockBottone) {
									view.getListaDomande().clear();
								}

								if (domanda.getReplica() == null) {
									view.getListaDomande().addItem(domanda.getDomanda(), "Nessuna risposta");
								} else {
									view.getListaDomande().addItem(domanda.getDomanda(),
											domanda.getReplica().getRisposta());
								}
								lockBottone = false;
							}

							if (!lockBottone) {
								view.getListaDomande().setSelectedIndex(0);
								view.getCampoDomanda()
										.setText("DOMANDA: " + view.getListaDomande().getSelectedItemText());
								view.getCampoRisposta()
										.setText("RISPOSTA: " + view.getListaDomande().getSelectedValue());
							}

							checkLockBottone();
						}
					}
				}
			}
		});

		view.getListaDomande().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!lockBottone) {
					view.getCampoDomanda().setText("DOMANDA: " + view.getListaDomande().getSelectedItemText());
					view.getCampoRisposta().setText("RISPOSTA: " + view.getListaDomande().getSelectedValue());
				}
			}
		});

		view.getEliminaDomandaButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QuestionAnswerServiceAsync eliminaDomanda = GWT.create(QuestionAnswerService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE ELIMINAZIONE DOMANDA:", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("ELIMINAZIONE DOMANDA AVVENUTA:", result);
					}
				};
				for (Oggetto oggetto : oggetti) {
					if (oggetto.getKey().equals(view.getOggettoSelezionato().getSelectedObject())) {
						for (Domanda domanda : oggetto.getDomande()) {
							if (view.getListaDomande().getSelectedItemText().equals(domanda.getDomanda())) {
								eliminaDomanda.deleteQuestion(oggetto.getKey(), domanda, callback);
							}
						}
					}
				}
			}
		});

		view.getEliminaRispostaButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QuestionAnswerServiceAsync eliminaRisposta = GWT.create(QuestionAnswerService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE ELIMINAZIONE RISPOSTA:", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("ELIMINAZIONE RISPOSTA AVVENUTA:", result);
					}
				};
				for (Oggetto oggetto : oggetti) {
					if (oggetto.getKey().equals(view.getOggettoSelezionato().getSelectedObject())) {
						for (Domanda domanda : oggetto.getDomande()) {
							if (view.getListaDomande().getSelectedItemText().equals(domanda.getDomanda())) {
								eliminaRisposta.deleteAnswer(oggetto.getKey(), domanda, callback);
							}
						}
					}
				}
			}
		});
	}

	private void checkLockBottone() {
		if (!lockBottone) {
			view.getEliminaDomandaButton().setEnabled(true);
			view.getEliminaRispostaButton().setEnabled(true);
		} else {
			view.getEliminaDomandaButton().setEnabled(false);
			view.getEliminaRispostaButton().setEnabled(false);
		}
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		container.clear();
		getListaOggetti();
		container.add(view.asWidget());

	}
}
