package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

public class NullPagePresenter implements Presenter {

	public interface Display {
	}

	public NullPagePresenter() {
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();

	}

	@Override
	public void bind() {
	}

}
