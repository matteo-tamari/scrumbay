package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ViewDetailsEvent;
import com.google.gwt.scrumbay.client.service.OnSaleService;
import com.google.gwt.scrumbay.client.service.OnSaleServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class OggettiInVenditaPresenter implements Presenter {

	public interface Display {
		public VerticalPanel getRootPanel();

		public LinkedList<Hyperlink> getObjectsLink();

		public FlexTable getObjectPanel();

		public Widget asWidget();
	}

	private final MessageBoxView messageBoxOnFailure;
	final private Display view;
	private SimpleEventBus eventBus;
	private String categoria;

	public OggettiInVenditaPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnFailure = new MessageBoxView();
		this.eventBus = eventBus;
		this.categoria = "NULL";
	}

	public OggettiInVenditaPresenter(Display view, SimpleEventBus eventBus, String categoria) {
		this(view, eventBus);
		this.categoria = categoria;
	}

	public void listOnSaleObjects(String category) {
		if (categoria.equals("NULL")) {
			OnSaleServiceAsync getOnSaleObject = GWT.create(OnSaleService.class);
			AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
				@Override
				public void onFailure(Throwable caught) {
					messageBoxOnFailure.showMessageBoxView("ERRORE NEL RECUPERO DEGLI OGGETTI IN VENDITA",
							caught.getMessage());
				}

				@SuppressWarnings("deprecation")
				@Override
				public void onSuccess(LinkedList<Oggetto> result) {
					int i;
					for (i = 0; i < result.size(); i++) {
						final Oggetto oggettoI = result.get(i);
						Hyperlink link = new Hyperlink(
								oggettoI.getCategoria().getNomeCategoria() + " " + oggettoI.getDataScadenza() + " "
										+ oggettoI.getNome() + " " + oggettoI.getVenditore().getUsername(),
								oggettoI.getKey());
						link.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								eventBus.fireEvent(new ViewDetailsEvent(oggettoI));
							}
						});
						view.getObjectsLink().add(link);
						view.getObjectPanel().setWidget(i, 1, link);
					}
				}
			};
			getOnSaleObject.getOnSaleObjects(callback);
		} else {
			OnSaleServiceAsync onSaleCategories = GWT.create(OnSaleService.class);
			AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@SuppressWarnings("deprecation")
				@Override
				public void onSuccess(LinkedList<Oggetto> result) {
					int i;
					for (i = 0; i < result.size(); i++) {
						final Oggetto oggettoI = result.get(i);
						Hyperlink link = new Hyperlink(
								oggettoI.getCategoria().getNomeCategoria() + " " + oggettoI.getDataScadenza() + " "
										+ oggettoI.getNome() + " " + oggettoI.getVenditore().getUsername(),
								oggettoI.getKey());
						link.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								eventBus.fireEvent(new ViewDetailsEvent(oggettoI));
							}
						});
						view.getObjectsLink().add(link);
						view.getObjectPanel().setWidget(i, 1, link);
					}
				}
			};
			onSaleCategories.getOnSaleObjectsForCategory(category, callback);
		}
	}

	@Override
	public void go(final HasWidgets container) {
		container.clear();
		listOnSaleObjects(categoria);
		container.add(view.asWidget());
	}

	@Override
	public void bind() {
	}
}
