package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.QuestionAnswerService;
import com.google.gwt.scrumbay.client.service.QuestionAnswerServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class RispostaOggettoPresenter implements Presenter {

	public interface Display {
		public SingleSelectionModel<String> getDomandaSelezionata();

		public SimplePager getPager();

		public CellList<String> getListaDomanda();

		public Label getDomandaLabel();

		public TextBox getCampoRisposta();

		public Button getConfirmButton();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private final Display view;
	private Domanda domandaSelezionata;
	private LinkedList<Domanda> domandePendenti;
	private LinkedList<String> listaDomande = new LinkedList<String>();
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private boolean rispostaOggettoBtnLock = false;
	private boolean selezioneDomandaLock = false;
	private final Oggetto oggetto;

	public RispostaOggettoPresenter(Display view, SimpleEventBus eventBus, Oggetto oggetto) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new ShowMainPageEvent());
		this.messageBoxOnFailure = new MessageBoxView();
		this.oggetto = oggetto;
	}

	private void getDomandeOggetto() {
		domandePendenti = oggetto.getDomandePendenti();
		for (int i = 0; i < domandePendenti.size(); i++) {
			listaDomande.add(domandePendenti.get(i).getDomanda());
		}
		view.getListaDomanda().setRowData(0, listaDomande);
		view.getListaDomanda().setVisibleRangeAndClearData(new Range(0, 3), true);
		view.getPager().setDisplay(view.getListaDomanda());
	}

	public void bind() {
		view.getListaDomanda().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				Range range = event.getNewRange();
				int start = range.getStart();
				int length = range.getLength();

				if (start >= domandePendenti.size()) {
					view.getPager().setPage(0);
				} else {

					if (start + length > domandePendenti.size() && domandePendenti.size() > start) {
						length = domandePendenti.size() - start;
					}

					LinkedList<String> data = new LinkedList<String>();
					for (int i = start; i < start + length; i++) {
						data.add(domandePendenti.get(i).getDomanda());
					}
					view.getListaDomanda().setRowData(start, data);
				}
			}
		});

		view.getDomandaSelezionata().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				selezioneDomandaLock = true;
				String selected = view.getDomandaSelezionata().getSelectedObject();
				if (selected != null) {
					for (Domanda domanda : oggetto.getDomande()) {
						if (domanda.getDomanda().equals(selected)) {
							domandaSelezionata = domanda;
							view.getDomandaLabel().setText(domanda.getDomanda());
						}
					}
				}
			}
		});

		view.getCampoRisposta().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (view.getCampoRisposta().getValue().isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Il campo non puo' essere vuoto ");
					rispostaOggettoBtnLock = false;
					checkOfferBuy();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Campo inserito correttamente");
					rispostaOggettoBtnLock = checkAnswer();
					checkOfferBuy();
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				QuestionAnswerServiceAsync rispostaSvc = GWT.create(QuestionAnswerService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE INSERIMENTO RISPOSTA: ", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("RISPOSTA INSERITA CORRETTAMENTE: ", result);
					}
				};
				rispostaSvc.saveAnswer(oggetto.getKey(), domandaSelezionata, view.getCampoRisposta().getText(),
						(UtenteVenditore) Sessione.getSessioneUtente(), callback);
			}
		});
	}

	public boolean checkAnswer() {
		if (view.getCampoRisposta().getText().length() <= 8) {
			return false;
		} else {
			return true;
		}
	}

	public void checkOfferBuy() {
		if (rispostaOggettoBtnLock && selezioneDomandaLock) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

	@Override
	public void go(HasWidgets container) {
		bind();
		container.clear();
		getDomandeOggetto();
		container.add(view.asWidget());
	}

}
