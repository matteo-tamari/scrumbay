package com.google.gwt.scrumbay.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.OfferEvent;
import com.google.gwt.scrumbay.client.service.DoOfferService;
import com.google.gwt.scrumbay.client.service.DoOfferServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class OffertaOggettoPresenter implements Presenter {

	public interface Display {
		public TextBox getCampoOffertaOggetto();

		public Button getConfirmButton();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private final Display view;
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private boolean offertaOggettoBtnLock = false;
	private final Oggetto oggetto;

	public OffertaOggettoPresenter(Display view, SimpleEventBus eventBus, Oggetto oggetto) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new OfferEvent());
		this.messageBoxOnFailure = new MessageBoxView();
		this.oggetto = oggetto;
	}

	public void bind() {
		view.getCampoOffertaOggetto().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (view.getCampoOffertaOggetto().getValue().isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Il campo non puo' essere vuoto ");
					offertaOggettoBtnLock = false;
					checkOfferBuy();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Campo inserito correttamente");
					offertaOggettoBtnLock = checkValueOffer();
					checkOfferBuy();
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				DoOfferServiceAsync offerta = GWT.create(DoOfferService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE INSERIMENTO OFFERTA: ", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("OFFERTA INSERITA CORRETTAMENTE: ", result);
					}
				};
				offerta.offer((UtenteAcquirente) Sessione.getSessioneUtente(),
						Double.valueOf(view.getCampoOffertaOggetto().getText()), oggetto, callback);
			}
		});
	}

	public boolean checkValueOffer() {
		if (Double.valueOf(view.getCampoOffertaOggetto().getText()) <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public void checkOfferBuy() {
		if (offertaOggettoBtnLock) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

	@Override
	public void go(HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
	}

}