package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.SellEvent;
import com.google.gwt.scrumbay.client.service.CategoryService;
import com.google.gwt.scrumbay.client.service.CategoryServiceAsync;
import com.google.gwt.scrumbay.client.service.SaleService;
import com.google.gwt.scrumbay.client.service.SaleServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class VenditaOggettoPresenter implements Presenter {

	public interface Display {

		public TextBox getCampoNomeOggetto();

		public RichTextArea getCampoDescrizioneOggetto();

		public TextBox getCampoPrezzoVendita();

		public TextBox getCampoDataScadenza();

		public ListBox getCampoCategoria();

		public Button getConfirmButton();

		public Label getErrorLabel();

		public Widget asWidget();
	}

	private final Display view;
	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	private boolean nomeOggettoBtnLock = false;
	private boolean descrizioneOggettoBtnLock = false;
	private boolean prezzoVenditaBtnLock = false;
	private boolean dataScadenzaBtnLock = false;
	private boolean btnLockCategoriaPadre = false;
	private LinkedList<Categoria> elencoCategorie;

	public VenditaOggettoPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new SellEvent());
		this.messageBoxOnFailure = new MessageBoxView();
		elencoCategorie = new LinkedList<Categoria>();
	}

	public void bind() {
		view.getCampoNomeOggetto().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (view.getCampoNomeOggetto().getValue().isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Il campo non puo' essere vuoto ");
					descrizioneOggettoBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Campo inserito correttamente");
					nomeOggettoBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoDescrizioneOggetto().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (view.getCampoDescrizioneOggetto().getText().isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Il campo non puo' essere vuoto ");
					descrizioneOggettoBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Campo inserito correttamente");
					descrizioneOggettoBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoPrezzoVendita().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoPrezzoVendita().getText();
				if (!input.matches("^\\d{0,8}(\\.\\d{1,4})?$") || input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Devi inserire un prezzo di vendita");
					prezzoVenditaBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Prezzo inserito correttamente");
					prezzoVenditaBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoDataScadenza().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				final String input = view.getCampoDataScadenza().getText();
				if (!input.matches(
						"^$|^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/]([0]?[1-9]|[1][0-2])[/]([1-2]{1}[0|9]{1}[0-9]{1}[0-9]{1})+$")
						|| input.isEmpty()) {
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("La data deve avere il formato gg/mm/aaaa");
					dataScadenzaBtnLock = false;
					checkTextBoxes();
				} else {
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Data di scadenza inserita correttamente.");
					dataScadenzaBtnLock = true;
					checkTextBoxes();
				}
			}
		});

		view.getCampoCategoria().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String categoriaPadre = view.getCampoCategoria().getSelectedValue();
				if (categoriaPadre.isEmpty()) {
					btnLockCategoriaPadre = false;
					view.getErrorLabel().getElement().getStyle().setColor("red");
					view.getErrorLabel().setText("Selezionare una categoria di riferimento.");
				} else {
					if (categoriaPadre.equals("NULL")) {
						btnLockCategoriaPadre = false;
					} else {
						btnLockCategoriaPadre = true;
					}
					view.getErrorLabel().getElement().getStyle().setColor("green");
					view.getErrorLabel().setText("Categoria di riferimento OK.");
					checkTextBoxes();
					aggiornaElencoCategorie(view.getCampoCategoria().getSelectedValue());
				}
			}
		});

		view.getConfirmButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

				UtenteVenditore venditore = (UtenteVenditore) Sessione.getSessioneUtente();
				String nome = view.getCampoNomeOggetto().getText();
				String descrizione = view.getCampoDescrizioneOggetto().getText();
				double prezzoBase = Double.valueOf(view.getCampoPrezzoVendita().getText());
				String dataScadenza = view.getCampoDataScadenza().getText();
				Categoria categoria = new Categoria();
				for (Categoria cat : elencoCategorie) {
					if (cat.getNomeCategoria().equals(view.getCampoCategoria().getSelectedValue())) {
						categoria = cat;
					}
					if (cat.getCategoriaPadre().getNomeCategoria()
							.equals(view.getCampoCategoria().getSelectedValue())) {
						categoria = cat.getCategoriaPadre();
					}
				}

				SaleServiceAsync venditaOggettoSvc = GWT.create(SaleService.class);

				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE MESSA IN VENDITA:", caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("MESSA IN VENDITA CONCLUSA:", result);
					}
				};
				venditaOggettoSvc.sellItem(venditore, nome, descrizione, prezzoBase, dataScadenza, categoria, callback);
			}
		});
	}

	private void aggiornaElencoCategorie(final String categoriaPadre) {
		CategoryServiceAsync categorieSvc = GWT.create(CategoryService.class);
		AsyncCallback<LinkedList<Categoria>> callback = new AsyncCallback<LinkedList<Categoria>>() {
			@Override
			public void onFailure(Throwable caught) {
				view.getErrorLabel().setText(caught.getMessage());
			}

			@Override
			public void onSuccess(LinkedList<Categoria> result) {
				view.getCampoCategoria().clear();
				if (categoriaPadre.equals("NULL")) {
					view.getCampoCategoria().addItem("- ELENCO CATEGORIE -", categoriaPadre);
					elencoCategorie = result;
				} else {
					view.getCampoCategoria().addItem(categoriaPadre, categoriaPadre);
					if (result.size() > 0) {
						view.getCampoCategoria().addItem("<-- Back",
								result.getLast().getCategoriaPadre().getCategoriaPadre().getNomeCategoria());
						elencoCategorie = result;
					} else {
						view.getCampoCategoria().addItem("<-- Back",
								elencoCategorie.getLast().getCategoriaPadre().getNomeCategoria());
					}
				}

				for (Categoria categoria : result) {
					view.getCampoCategoria().addItem("     " + categoria.getNomeCategoria(),
							categoria.getNomeCategoria());
				}
			}

		};
		categorieSvc.getCategories(categoriaPadre, callback);
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		container.clear();
		container.add(view.asWidget());
		aggiornaElencoCategorie("NULL");
	}

	public void checkTextBoxes() {
		if (btnLockCategoriaPadre && nomeOggettoBtnLock && descrizioneOggettoBtnLock && prezzoVenditaBtnLock
				&& dataScadenzaBtnLock) {
			view.getConfirmButton().setEnabled(true);
		} else {
			view.getConfirmButton().setEnabled(false);
		}
	}

}
