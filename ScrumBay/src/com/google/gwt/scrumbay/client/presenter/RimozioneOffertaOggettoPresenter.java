package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.service.OnSaleService;
import com.google.gwt.scrumbay.client.service.OnSaleServiceAsync;
import com.google.gwt.scrumbay.client.service.RemoveObjectService;
import com.google.gwt.scrumbay.client.service.RemoveObjectServiceAsync;
import com.google.gwt.scrumbay.client.service.RemoveOfferService;
import com.google.gwt.scrumbay.client.service.RemoveOfferServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class RimozioneOffertaOggettoPresenter implements Presenter {

	public interface Display {
		public VerticalPanel getRootPanel();

		public Button getDeleteOfferButton();

		public Button getDeleteItemButton();

		public LinkedList<RadioButton> getRadioGroup();

		public FlexTable getObjectPanel();

		public Widget asWidget();
	}

	private final MessageBoxView messageBoxOnSuccess;
	private final MessageBoxView messageBoxOnFailure;
	final private Display view;

	public RimozioneOffertaOggettoPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnSuccess = new MessageBoxView(eventBus, new ShowMainPageEvent());
		this.messageBoxOnFailure = new MessageBoxView();
	}

	public void listOnSaleObjects() {

		OnSaleServiceAsync getOnSaleObject = GWT.create(OnSaleService.class);
		AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				messageBoxOnFailure.showMessageBoxView("ERRORE NEL RECUPERO DEGLI OGGETTI IN VENDITA",
						caught.getMessage());
			}

			@Override
			public void onSuccess(LinkedList<Oggetto> result) {
				int i;
				for (i = 0; i < result.size(); i++) {
					Oggetto oggettoI = result.get(i);
					view.getObjectPanel().setText(i, 0, oggettoI.getKey());
					RadioButton radio = new RadioButton("group");
					view.getRadioGroup().add(radio);
					view.getObjectPanel().setWidget(i, 1, radio);
				}
				view.getObjectPanel().setWidget(i, 0, view.getDeleteItemButton());
				view.getObjectPanel().setWidget(i, 1, view.getDeleteOfferButton());
			}
		};
		getOnSaleObject.getOnSaleObjects(callback);
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		container.clear();
		listOnSaleObjects();
		container.add(view.asWidget());
	}

	@Override
	public void bind() {
		view.getDeleteOfferButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RemoveOfferServiceAsync removeOffer = GWT.create(RemoveOfferService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE: cancellazione offerta non avvenuta",
								caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("SUCCESSO:", "Hai cancellato l'offerta con successo!");
					}
				};
				int i = 0;
				for (RadioButton radio : view.getRadioGroup()) {
					if (radio.getValue()) {
						removeOffer.removeOffer(view.getObjectPanel().getText(i, 0), callback);
						break;
					}
					i++;
				}
			}
		});

		view.getDeleteItemButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RemoveObjectServiceAsync removeObject = GWT.create(RemoveObjectService.class);
				AsyncCallback<String> callback = new AsyncCallback<String>() {
					@Override
					public void onFailure(Throwable caught) {
						messageBoxOnFailure.showMessageBoxView("ERRORE: cancellazione oggetto non avvenuta",
								caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						messageBoxOnSuccess.showMessageBoxView("SUCCESSO:", result);
					}
				};
				int i = 0;
				for (RadioButton radio : view.getRadioGroup()) {
					if (radio.getValue()) {
						removeObject.removeObject(view.getObjectPanel().getText(i, 0), callback);
						break;
					}
					i++;
				}
			}
		});
	}

}
