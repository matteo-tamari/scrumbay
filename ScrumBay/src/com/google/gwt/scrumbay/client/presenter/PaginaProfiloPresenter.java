package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.ViewDetailsEvent;
import com.google.gwt.scrumbay.client.service.SoldAndOfferService;
import com.google.gwt.scrumbay.client.service.SoldAndOfferServiceAsync;
import com.google.gwt.scrumbay.client.view.MessageBoxView;
import com.google.gwt.scrumbay.shared.Offerta;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.Widget;

public class PaginaProfiloPresenter implements Presenter {

	public interface Display {
		public Label getUsername();

		public Label getNome();

		public Label getCognome();

		public Label getSesso();

		public Label getDataNascita();

		public Label getLuogoNascita();

		public Label getCodiFiscale();

		public Label getTelefono();

		public Label getEmail();

		public Label getDomicilio();

		public FlexTable myOnSaleObjects();

		public FlexTable objectsIBidOn();

		public TabPanel getInformationPanel();

		public Widget asWidget();
	}

	private final MessageBoxView messageBoxOnFailure;
	final private Display view;
	private SimpleEventBus eventBus;
	private UtenteRegistrato utenteSessione = (UtenteRegistrato) Sessione.getSessioneUtente();

	public PaginaProfiloPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.messageBoxOnFailure = new MessageBoxView();
		this.eventBus = eventBus;
	}

	public void returnUserData() {
		view.getUsername().setText(utenteSessione.getUsername());
		view.getNome().setText(utenteSessione.getNome());
		view.getCognome().setText(utenteSessione.getCognome());
		view.getSesso().setText(utenteSessione.getSesso());
		view.getDataNascita().setText(utenteSessione.getDataNascita());
		view.getLuogoNascita().setText(utenteSessione.getLuogoNascita());
		view.getCodiFiscale().setText(utenteSessione.getCodiceFiscale());
		view.getTelefono().setText(utenteSessione.getTelefono());
		view.getEmail().setText(utenteSessione.getEmail());
		view.getDomicilio().setText(utenteSessione.getDomicilio());
	}

	public void listOnSaleObjects() {
		SoldAndOfferServiceAsync getMySoldObjects = GWT.create(SoldAndOfferService.class);
		AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				messageBoxOnFailure.showMessageBoxView("ERRORE NEL RECUPERO DEGLI OGGETTI IN VENDITA",
						caught.getMessage());
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onSuccess(LinkedList<Oggetto> result) {
				int i;
				for (i = 0; i < result.size(); i++) {
					final Oggetto oggettoI = result.get(i);
					Hyperlink link = new Hyperlink(oggettoI.getCategoria().getNomeCategoria() + " "
							+ oggettoI.getDataScadenza() + " " + oggettoI.getNome() + " "
							+ oggettoI.getVenditore().getUsername() + " - " + oggettoI.getStato(), oggettoI.getKey());
					link.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							eventBus.fireEvent(new ViewDetailsEvent(oggettoI));
						}
					});
					view.myOnSaleObjects().setWidget(i, 0, link);
				}
			}
		};
		getMySoldObjects.getMySoldObjects(utenteSessione, callback);
	}

	// recupero le informazioni degli oggetti alla quele e' stata fatta una offerta
	public void listOnOfferObjects() {
		SoldAndOfferServiceAsync myOffersService = GWT.create(SoldAndOfferService.class);
		AsyncCallback<LinkedList<Oggetto>> callback = new AsyncCallback<LinkedList<Oggetto>>() {
			@Override
			public void onFailure(Throwable caught) {
				messageBoxOnFailure.showMessageBoxView("ERRORE NEL RECUPERO DELLE OFFERTE FATTE", caught.getMessage());
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onSuccess(LinkedList<Oggetto> result) {
				int i = 0;
				for (final Oggetto oggettoI : result) {
					Offerta lastOffer = null;
					for (Offerta offer : oggettoI.getOfferte()) {
						if (offer.getAcquirente().getUsername().equals(utenteSessione.getUsername())) {
							lastOffer = offer;
						}
					}
					Hyperlink link = new Hyperlink(oggettoI.getVenditore().getUsername() + " " + oggettoI.getNome()
							+ " " + lastOffer.getImporto() + " - " + lastOffer.getStato(), oggettoI.getKey());
					link.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							eventBus.fireEvent(new ViewDetailsEvent(oggettoI));
						}
					});
					view.objectsIBidOn().setWidget(i, 0, link);
					i++;
				}
			}
		};
		myOffersService.getObjectsWithMyBid(utenteSessione, callback);
	}

	@Override
	public void go(HasWidgets container) {
		returnUserData();
		listOnSaleObjects();
		listOnOfferObjects();
		container.clear();
		container.add(view.asWidget());
	}

	@Override
	public void bind() {
	}

}
