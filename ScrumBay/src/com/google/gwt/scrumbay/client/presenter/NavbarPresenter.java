package com.google.gwt.scrumbay.client.presenter;

import java.util.LinkedList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.DeleteAnswerEvent;
import com.google.gwt.scrumbay.client.event.DeleteQuestionEvent;
import com.google.gwt.scrumbay.client.event.LoginEvent;
import com.google.gwt.scrumbay.client.event.LogoutEvent;
import com.google.gwt.scrumbay.client.event.NewCategoryEvent;
import com.google.gwt.scrumbay.client.event.RegistrationEvent;
import com.google.gwt.scrumbay.client.event.RenameCategoryEvent;
import com.google.gwt.scrumbay.client.event.SellEvent;
import com.google.gwt.scrumbay.client.event.ShowMainPageEvent;
import com.google.gwt.scrumbay.client.event.ShowPersonalPageEvent;
import com.google.gwt.scrumbay.client.service.CategoryService;
import com.google.gwt.scrumbay.client.service.CategoryServiceAsync;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

public class NavbarPresenter implements Presenter {

	public interface Display {
		public Button getLoginButton();

		public Button getLogoutButton();

		public Button getRegistrationButton();

		public Button getProfileButton();

		public Button getSellButton();

		public Button getLogoButton();

		public ListBox getCategoriesListbox();

		public ListBox getManageCategoryButton();

		public ListBox getManageQuestionAndAnswer();

		Widget asWidget();
	}

	private final SimpleEventBus eventBus;
	private final Display view;
	private LinkedList<Categoria> elencoCategorie;

	public NavbarPresenter(Display view, SimpleEventBus eventBus) {
		this.view = view;
		this.eventBus = eventBus;
		elencoCategorie = new LinkedList<Categoria>();
	}

	@Override
	public void bind() {

		view.getLoginButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new LoginEvent());
			}
		});

		view.getLogoutButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new LogoutEvent());
			}
		});

		view.getRegistrationButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new RegistrationEvent());
			}
		});

		view.getSellButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new SellEvent());
			}
		});

		view.getLogoButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new ShowMainPageEvent());
			}
		});

		view.getManageCategoryButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (view.getManageCategoryButton().isItemSelected(1)) {
					view.getManageCategoryButton().setSelectedIndex(0);
					eventBus.fireEvent(new NewCategoryEvent());
				}
				if (view.getManageCategoryButton().isItemSelected(2)) {
					view.getManageCategoryButton().setSelectedIndex(0);
					eventBus.fireEvent(new RenameCategoryEvent());
				}
			}
		});

		view.getManageQuestionAndAnswer().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (view.getManageQuestionAndAnswer().isItemSelected(1)) {
					view.getManageQuestionAndAnswer().setSelectedIndex(0);
					eventBus.fireEvent(new DeleteQuestionEvent());
				}
				if (view.getManageQuestionAndAnswer().isItemSelected(2)) {
					view.getManageQuestionAndAnswer().setSelectedIndex(0);
					eventBus.fireEvent(new DeleteAnswerEvent());
				}
			}
		});

		view.getProfileButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new ShowPersonalPageEvent());
			}
		});

		view.getCategoriesListbox().addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				String selectedCategory = view.getCategoriesListbox().getSelectedValue();
				aggiornaElencoCategorie(selectedCategory);
				if (!selectedCategory.isEmpty()) {
					eventBus.fireEvent(new ShowMainPageEvent(view.getCategoriesListbox().getSelectedValue()));
				}
			}
		});
	}

	private void aggiornaElencoCategorie(final String categoriaPadre) {
		CategoryServiceAsync categorieSvc = GWT.create(CategoryService.class);
		AsyncCallback<LinkedList<Categoria>> callback = new AsyncCallback<LinkedList<Categoria>>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(LinkedList<Categoria> result) {
				view.getCategoriesListbox().clear();
				if (categoriaPadre.equals("NULL")) {
					view.getCategoriesListbox().addItem("- ELENCO CATEGORIE -", categoriaPadre);
					elencoCategorie = result;
				} else {
					view.getCategoriesListbox().addItem(categoriaPadre, categoriaPadre);
					if (result.size() > 0) {
						view.getCategoriesListbox().addItem("<-- Back",
								result.getLast().getCategoriaPadre().getCategoriaPadre().getNomeCategoria());
						elencoCategorie = result;
					} else {
						view.getCategoriesListbox().addItem("<-- Back",
								elencoCategorie.getLast().getCategoriaPadre().getNomeCategoria());
					}
				}
				for (Categoria categoria : result) {
					view.getCategoriesListbox().addItem("     " + categoria.getNomeCategoria(),
							categoria.getNomeCategoria());
				}
			}
		};
		categorieSvc.getCategories(categoriaPadre, callback);
	}

	@Override
	public void go(final HasWidgets container) {
		bind();
		aggiornaElencoCategorie("NULL");
		container.clear();
		container.add(view.asWidget());
	}
}
