package com.google.gwt.scrumbay.client;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.scrumbay.client.event.*;
import com.google.gwt.scrumbay.client.presenter.*;
import com.google.gwt.scrumbay.client.view.*;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.Sessione;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.scrumbay.shared.UtenteVisitatore;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.RootPanel;

public class AppController {

	/**
	 * Questa classe contiene l'app controller, ossia la logica non specifica a
	 * nessun presenter. Un esempio di cio' possono essere le prime operazioni di
	 * inizializzazione delle pagine e dei presenter all'apertura del sito
	 */
	private final SimpleEventBus eventBus;
	private Presenter presenter;
	private HasWidgets panelModifica;

	public AppController(SimpleEventBus eventBus) {
		this.eventBus = eventBus;
		bind();
	}

	private void bind() {
		eventBus.addHandler(NullPageEvent.TYPE, new NullPageEventHandler() {
			@Override
			public void onNullRequest(NullPageEvent event) {
				// Quello che deve essere fatto al momento del catch di questo evento
				showNullPage();
			}
		});

		eventBus.addHandler(RegistrationEvent.TYPE, new RegistrationEventHandler() {
			@Override
			public void onNewRegistration(RegistrationEvent event) {
				showRegistrationForm();
			}
		});

		eventBus.addHandler(LoginEvent.TYPE, new LoginEventHandler() {
			@Override
			public void onNewLogin(LoginEvent event) {
				showLoginForm();
			}
		});

		eventBus.addHandler(LogoutEvent.TYPE, new LogoutEventHandler() {
			@Override
			public void onLogout(LogoutEvent event) {
				Sessione.setSessioneUtente(new UtenteVisitatore());
				showMainPage("NULL");
			}
		});

		eventBus.addHandler(SellEvent.TYPE, new SellEventHandler() {
			@Override
			public void onNewSell(SellEvent event) {
				showSellForm();
			}
		});

		eventBus.addHandler(OfferEvent.TYPE, new OfferEventHandler() {
			@Override
			public void onNewOffer(OfferEvent offer) {
				showMainPage("NULL");
			}
		});

		eventBus.addHandler(RenameCategoryEvent.TYPE, new RenameCategoryHandler() {
			@Override
			public void onRename(RenameCategoryEvent rename) {
				showRenameCategoryPage();
			}
		});

		eventBus.addHandler(NewCategoryEvent.TYPE, new NewCategoryHandler() {
			@Override
			public void onNewCategory(NewCategoryEvent offer) {
				showNewCategoryPage();
			}
		});
		eventBus.addHandler(ShowMainPageEvent.TYPE, new ShowMainPageHandler() {
			@Override
			public void onNewRefresh(ShowMainPageEvent event) {
				if (event.getCategory() != null) {
					showMainPage(event.getCategory());
				} else {
					showMainPage("NULL");
				}
			}
		});

		eventBus.addHandler(ViewDetailsEvent.TYPE, new ViewDetailsEventHandler() {
			@Override
			public void onDetailsRequest(ViewDetailsEvent event) {
				showDetailsPage(event.getOggettoDaVisualizzare());
			}
		});
		eventBus.addHandler(DeleteQuestionEvent.TYPE, new DeleteQuestionEventHandler() {
			@Override
			public void onDeleteQuestion(DeleteQuestionEvent event) {
				showManageQuestionsPage(0);
			}
		});

		eventBus.addHandler(DeleteAnswerEvent.TYPE, new DeleteAnswerEventHandler() {
			@Override
			public void onDeleteAnswer(DeleteAnswerEvent event) {
				showManageQuestionsPage(1);
			}
		});

		eventBus.addHandler(ShowPersonalPageEvent.TYPE, new ShowPersonalPageHandler() {
			@Override
			public void onPersonalPageRequest(ShowPersonalPageEvent event) {
				showPersonalPage();
			}
		});
	}

	protected void showManageQuestionsPage(int typePage) {
		RootPanel.get("figureContainer").setVisible(false);

		this.presenter = new NavbarPresenter(new NavbarView(), eventBus);
		panelModifica = RootPanel.get("navbarSupportedContent");
		presenter.go(panelModifica);

		this.presenter = new RimozioneDomandaRispostaPresenter(new RimozioneDomandaRispostaView(typePage), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showNullPage() {
		RootPanel.get("figureContainer").setVisible(false);

		this.presenter = new NullPagePresenter();
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showRegistrationForm() {
		RootPanel.get("figureContainer").setVisible(true);

		this.presenter = new RegistrationPresenter(new RegistrationView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showLoginForm() {
		RootPanel.get("figureContainer").setVisible(true);

		this.presenter = new LoginPresenter(new LoginView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showSellForm() {
		RootPanel.get("figureContainer").setVisible(true);

		this.presenter = new VenditaOggettoPresenter(new VenditaOggettoView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		Sessione.setSessioneUtente(new UtenteVenditore((UtenteRegistrato) Sessione.getSessioneUtente()));
		presenter.go(panelModifica);
	}

	private void showRenameCategoryPage() {
		RootPanel.get("figureContainer").setVisible(true);

		this.presenter = new RenameCategoryPresenter(new RenameCategoryView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);

	}

	private void showNewCategoryPage() {
		RootPanel.get("figureContainer").setVisible(true);

		this.presenter = new NuovaCategoriaPresenter(new NuovaCategoriaView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showMainPage(String category) {
		RootPanel.get("figureContainer").setVisible(false);
		if (category.equals("NULL")) {
			this.presenter = new NavbarPresenter(new NavbarView(), eventBus);
			panelModifica = RootPanel.get("navbarSupportedContent");
			presenter.go(panelModifica);
		}
		if (Sessione.getSessioneUtente() instanceof UtenteAmministratore) {
			this.presenter = new RimozioneOffertaOggettoPresenter(new RimozioneOffertaOggettoView(), eventBus);
		} else {
			if (category.equals("NULL")) {
				// Visualizzazione pagina oggetti in vendita senza categoria oggetti selezionata
				this.presenter = new OggettiInVenditaPresenter(new OggettiInVenditaView(), eventBus);
			} else {
				// Visualizzazione pagina oggetti in vendita con categoria scelta dall'utente
				this.presenter = new OggettiInVenditaPresenter(new OggettiInVenditaView(), eventBus, category);
			}
		}
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	private void showDetailsPage(Oggetto oggettoDaVisualizzare) {
		if (!(Sessione.getSessioneUtente() instanceof UtenteVisitatore)) {
			UtenteRegistrato utente = (UtenteRegistrato) Sessione.getSessioneUtente();
			if (oggettoDaVisualizzare.getVenditore().getUsername().equals(utente.getUsername())) {
				Sessione.setSessioneUtente(new UtenteVenditore((UtenteRegistrato) Sessione.getSessioneUtente()));
			} else {
				Sessione.setSessioneUtente(new UtenteAcquirente((UtenteRegistrato) Sessione.getSessioneUtente()));
			}
		}
		RootPanel.get("figureContainer").setVisible(false);
		this.presenter = new InformazioniOggettoPresenter(new InformazioniOggettoView(), eventBus,
				oggettoDaVisualizzare);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}

	public void showPersonalPage() {
		RootPanel.get("figureContainer").setVisible(false);
		this.presenter = new PaginaProfiloPresenter(new PaginaProfiloView(), eventBus);
		panelModifica = RootPanel.get("corpoPagina");
		presenter.go(panelModifica);
	}
}