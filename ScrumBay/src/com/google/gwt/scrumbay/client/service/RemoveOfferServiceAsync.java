package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RemoveOfferServiceAsync {

	void removeOffer(String objectKey, AsyncCallback<String> callback) throws IllegalArgumentException;

}
