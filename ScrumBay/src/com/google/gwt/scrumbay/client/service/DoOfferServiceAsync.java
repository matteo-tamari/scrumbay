package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DoOfferServiceAsync {

	void offer(UtenteAcquirente acquirente, double importo, Oggetto oggetto, AsyncCallback<String> callback);

}
