package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.Categoria;

@RemoteServiceRelativePath("category")
public interface CategoryService extends RemoteService {
	public String addNewCategory(String categoriaPadre, String nuovaCategoria) throws IllegalArgumentException;

	public LinkedList<Categoria> getCategories(String categoriaPadre) throws IllegalArgumentException;

	public String renameCategory(String nomeCategoria, String nuovoNomeCategoria) throws IllegalArgumentException;
}
