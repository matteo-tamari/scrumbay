package com.google.gwt.scrumbay.client.service;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SoldAndOfferServiceAsync {
	void getMySoldObjects(UtenteRegistrato user, AsyncCallback<LinkedList<Oggetto>> callback)
			throws IllegalArgumentException;

	void getObjectsWithMyBid(UtenteRegistrato user, AsyncCallback<LinkedList<Oggetto>> callback)
			throws IllegalArgumentException;
}
