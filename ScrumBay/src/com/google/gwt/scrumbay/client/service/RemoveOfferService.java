package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("removeoffer")
public interface RemoveOfferService extends RemoteService {

	public String removeOffer(String objectKey) throws IllegalArgumentException;

}
