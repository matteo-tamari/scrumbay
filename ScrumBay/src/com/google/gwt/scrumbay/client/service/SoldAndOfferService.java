package com.google.gwt.scrumbay.client.service;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("soldAndOfferInfo")
public interface SoldAndOfferService extends RemoteService {
	public LinkedList<Oggetto> getMySoldObjects(UtenteRegistrato user) throws IllegalArgumentException;

	public LinkedList<Oggetto> getObjectsWithMyBid(UtenteRegistrato user) throws IllegalArgumentException;

}
