
package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("registration")
public interface RegistrationService extends RemoteService {

	public String addRegistration(String username, String nome, String cognome, String telefono, String password,
			String email, String codiceFiscale, String domicilio, String sesso, String dataNascita, String luogoNascita)
			throws IllegalArgumentException;

}
