package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("login")
public interface LoginService extends RemoteService {

	public Utente signIn(String username, String password) throws IllegalArgumentException;

}
