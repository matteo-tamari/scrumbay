package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("removeobject")
public interface RemoveObjectService extends RemoteService {
	public String removeObject(String objectKey) throws IllegalArgumentException;
}
