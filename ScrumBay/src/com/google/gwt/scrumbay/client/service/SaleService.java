package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("venditaOggetto")
public interface SaleService extends RemoteService {
	public String sellItem(UtenteVenditore venditore, String nome, String descrizione, double prezzoBase,
			String dataScadenza, Categoria categoria) throws IllegalArgumentException;
}
