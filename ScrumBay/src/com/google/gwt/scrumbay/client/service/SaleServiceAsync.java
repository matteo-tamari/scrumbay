package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SaleServiceAsync {
	void sellItem(UtenteVenditore venditore, String nome, String descrizione, double prezzoBase, String dataScadenza,
			Categoria categoria, AsyncCallback<String> callback) throws IllegalArgumentException;
}
