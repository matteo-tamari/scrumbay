package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("manageQuestionAnswer")
public interface QuestionAnswerService extends RemoteService {
	public String newQuestion(String oggettoKey, Domanda domanda) throws IllegalArgumentException;

	public String deleteAnswer(String oggettoKey, Domanda domanda) throws IllegalArgumentException;

	public String deleteQuestion(String oggettoKey, Domanda domanda) throws IllegalArgumentException;

	public String saveAnswer(String oggettoKey, Domanda domanda, String risposta, UtenteVenditore rispondente)
			throws IllegalArgumentException;
}
