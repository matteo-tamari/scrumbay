package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LoginServiceAsync {

	void signIn(String username, String password, AsyncCallback<Utente> callback);

}
