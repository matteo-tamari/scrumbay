package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RemoveObjectServiceAsync {
	void removeObject(String objectKey, AsyncCallback<String> callback) throws IllegalArgumentException;
}
