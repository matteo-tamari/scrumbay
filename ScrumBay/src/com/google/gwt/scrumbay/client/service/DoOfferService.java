package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("offer")
public interface DoOfferService extends RemoteService {

	public String offer(UtenteAcquirente acquirente, double importo, Oggetto oggetto) throws IllegalArgumentException;
}
