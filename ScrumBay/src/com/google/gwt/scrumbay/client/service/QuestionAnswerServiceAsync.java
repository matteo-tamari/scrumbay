package com.google.gwt.scrumbay.client.service;

import com.google.gwt.scrumbay.shared.Domanda;
import com.google.gwt.scrumbay.shared.UtenteVenditore;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface QuestionAnswerServiceAsync {
	public void newQuestion(String oggettoKey, Domanda domanda, AsyncCallback<String> callback);

	public void deleteAnswer(String oggettoKey, Domanda domanda, AsyncCallback<String> callback);

	public void deleteQuestion(String oggettoKey, Domanda domanda, AsyncCallback<String> callback);

	public void saveAnswer(String oggettoKey, Domanda domanda, String risposta, UtenteVenditore rispondente,
			AsyncCallback<String> callback);
}
