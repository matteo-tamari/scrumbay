package com.google.gwt.scrumbay.client.service;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface OnSaleServiceAsync {

	void getOnSaleObjects(AsyncCallback<LinkedList<Oggetto>> callback);

	void getOnSaleObjectsForCategory(String categoriaPadre, AsyncCallback<LinkedList<Oggetto>> callback)
			throws IllegalArgumentException;
}
