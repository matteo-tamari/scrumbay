
package com.google.gwt.scrumbay.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RegistrationServiceAsync {

	void addRegistration(String username, String nome, String cognome, String telefono, String password, String email,
			String codiceFiscale, String domicilio, String sesso, String dataNascita, String luogoNascita,
			AsyncCallback<String> callback) throws IllegalArgumentException;

}
