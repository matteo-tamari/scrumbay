package com.google.gwt.scrumbay.client.service;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("onsale")
public interface OnSaleService extends RemoteService {
	public LinkedList<Oggetto> getOnSaleObjects();

	public LinkedList<Oggetto> getOnSaleObjectsForCategory(String categoriaPadre) throws IllegalArgumentException;
}