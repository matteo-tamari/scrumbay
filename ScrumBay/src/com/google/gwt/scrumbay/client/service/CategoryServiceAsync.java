package com.google.gwt.scrumbay.client.service;

import java.util.LinkedList;

import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CategoryServiceAsync {
	void addNewCategory(String categoriaPadre, String nuovaCategoria, AsyncCallback<String> callback)
			throws IllegalArgumentException;

	void getCategories(String categoriaPadre, AsyncCallback<LinkedList<Categoria>> callback)
			throws IllegalArgumentException;

	void renameCategory(String nomeCategoria, String nuovoNomeCategoria, AsyncCallback<String> callback)
			throws IllegalArgumentException;
}
