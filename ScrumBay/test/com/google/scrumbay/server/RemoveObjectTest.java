package com.google.scrumbay.server;

import static org.junit.Assert.fail;

import java.util.Date;
import java.util.LinkedList;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.RemoveObjectServiceImpl;
import com.google.gwt.scrumbay.shared.Oggetto;

public class RemoveObjectTest {
	DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	RemoveObjectServiceImpl service = new RemoveObjectServiceImpl();

	private void cleanDB() {
		VenditaOggettoTest test = new VenditaOggettoTest();
		forSale.clear();
		allObjects.clear();
		test.testAll();
	}

	@Test
	/**
	 * Questo test avviene dopo la pulizia del db, che chiama la procedura per
	 * mettere in vendita nuovi oggetti con scadenza domani. Dopodiche' si itera
	 * usando tutti i nomi degli oggetti attualmente in vendita dunque nel db
	 * dovranno rimanere 0 oggetti.
	 */
	public void deleteObjects() {
		cleanDB();
		int totalObjectsBeforeDeletion = allObjects.getKeys().size();
		for (String objectKey : allObjects.getKeys()) {
			service.removeObject(objectKey);
		}
		int totalObjectsAfterDeletion = allObjects.getKeys().size();
		if (totalObjectsBeforeDeletion <= totalObjectsAfterDeletion) {
			fail("Gli oggetti non sono stati eliminati correttamente");
		}
		for (Date day : forSale.getKeys()) {
			if (forSale.get(day).size() > 0) {
				fail("Gli oggetti di questo giorno non sono stati eliminati correttamente");
			}
		}
	}
}
