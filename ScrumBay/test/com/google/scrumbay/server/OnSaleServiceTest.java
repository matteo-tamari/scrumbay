package com.google.scrumbay.server;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.OnSaleServiceImpl;
import com.google.gwt.scrumbay.server.UtenteFactory;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.UtenteVenditore;

class OnSaleServiceTest {

	private OnSaleServiceImpl onSale = new OnSaleServiceImpl();
	private DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private HTreeMap<Date, LinkedList<Oggetto>> forSale = db.hashMap("DayOrdered", Serializer.DATE, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();

	private void dateTest() {
		forSale.clear();
		CategoryServiceTest categories = new CategoryServiceTest();
		categories.inserimentoNuoveCategorie();
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("lore", "Lore", "Fissagatti", "0284652822",
				"Test3", "lorenz@ping.it", "SFBDMO98F01A984I", "Via Lupo 101, Milano", "M", "01/04/1998", "Roma");
		Categoria laptop = mapCategorie.get("Laptop");
		UtenteVenditore user2 = new UtenteVenditore(user);
		Date today = new Date();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String todayAsString = format.format(today);
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		Date yesterday = cal.getTime();
		String yesterdayAsString = format.format(yesterday);
		cal.setTime(today);
		cal.add(Calendar.DATE, 1);
		Date tomorrow = cal.getTime();

		String tomorrowAsString = format.format(yesterday);
		Oggetto oggetto1 = new Oggetto(user2, "Dell xps 13",
				"Cedo Dell xps 13, perfette condizioni, da sostituire solo la batteria autonomia residua circa 3 ore ",
				578.56, yesterdayAsString, laptop);
		LinkedList<Oggetto> auctionExpired = new LinkedList<Oggetto>();
		auctionExpired.add(oggetto1);
		allObjects.put("TestVecchiaData", oggetto1);
		forSale.put(yesterday, auctionExpired);
		laptop.aggiungiOggetto(oggetto1);
		mapCategorie.put(laptop.getNomeCategoria(), laptop);

		Oggetto oggetto2 = new Oggetto(user2, "Thinkpad", "Cedo thinkpad, perfette condizioni", 578.56, todayAsString,
				laptop);
		LinkedList<Oggetto> auctionExpired2 = new LinkedList<Oggetto>();
		auctionExpired2.add(oggetto2);
		allObjects.put("TestDataCorrente", oggetto2);
		forSale.put(today, auctionExpired2);
		laptop.aggiungiOggetto(oggetto2);
		mapCategorie.put(laptop.getNomeCategoria(), laptop);

		Oggetto oggetto3 = new Oggetto(user2, "Mi notebook air", "Cedo mi air, schermo da cambiare.", 578.56,
				tomorrowAsString, laptop);
		LinkedList<Oggetto> ongoingAuction = new LinkedList<Oggetto>();
		ongoingAuction.add(oggetto3);
		allObjects.put("TestDataFutura", oggetto3);
		forSale.put(tomorrow, auctionExpired);
		laptop.aggiungiOggetto(oggetto3);
		mapCategorie.put(laptop.getNomeCategoria(), laptop);

		LinkedList<Oggetto> onSaleObjects = onSale.getOnSaleObjects();
		if (onSaleObjects.size() > 1) {
			fail("Sono stati ritornati anche oggetti scaduti");
		}
	}

	private void categoryTest() {
		Categoria rootCategory = mapCategorie.get("Laptop");
		LinkedList<Oggetto> onSaleThisCategory = onSale.getOnSaleObjectsForCategory("NULL");
		for (Oggetto object : onSaleThisCategory) {
			if (!(object.getCategoria().getNomeCategoria().equals(rootCategory.getNomeCategoria()))) {
				fail("Sono stati ritornati anche oggetti non appartenenti a questa categoria o a una sua sottostante");
			}
		}
	}

	@Test
	public void testAll() {
		dateTest();
		categoryTest();
	}

}
