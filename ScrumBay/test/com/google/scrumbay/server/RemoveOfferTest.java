package com.google.scrumbay.server;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Set;

import org.junit.jupiter.api.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.RemoveOfferServiceImpl;
import com.google.gwt.scrumbay.shared.Oggetto;

class RemoveOfferTest {
	RemoveOfferServiceImpl service = new RemoveOfferServiceImpl();
	DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	HTreeMap<String, Oggetto> allObjects = db.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();

	private void cleanDB() {
		DoOfferServiceTest sellAndOffer = new DoOfferServiceTest();
		sellAndOffer.testAll();
	}

	@Test
	void removeOffer() {
		cleanDB();
		Set<String> keys = allObjects.getKeys();
		for (String key : keys) {
			int offersBeforeRemoval = allObjects.get(key).getNumeroOfferte();
			service.removeOffer(allObjects.get(key).getKey());
			int offersAfterRemoval = allObjects.get(key).getNumeroOfferte();
			if (offersAfterRemoval == offersBeforeRemoval && offersBeforeRemoval != 0) {
				fail("Errore nella rimozione");
			}
		}

	}

}
