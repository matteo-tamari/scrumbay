package com.google.scrumbay.server;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.RegistrationServiceImpl;
import com.google.gwt.scrumbay.shared.Utente;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;

class RegistrationServiceTest {

	RegistrationServiceImpl service = new RegistrationServiceImpl();
	DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	HTreeMap<String, Utente> mapUtenti = db.hashMap("UtenteRegistrato", Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();

	HTreeMap<String, String> mapEmailRegistrate = db.hashMap("EmailRegistrate", Serializer.STRING, Serializer.STRING)
			.counterEnable().createOrOpen();

	HTreeMap<String, String> mapCFRegistrati = db.hashMap("CFRegistrati", Serializer.STRING, Serializer.STRING)
			.counterEnable().createOrOpen();

	HTreeMap<String, byte[]> mapSpeziale = db.hashMap("ZonaSpezie", Serializer.STRING, Serializer.BYTE_ARRAY)
			.counterEnable().createOrOpen();

	// SALVATAGGIO ED INSERIMENTO NEL DB DI NUOVI UTENTI SIA REGISTRATI CHE
	// AMMINISTRATORI TRAMITE PROCEDURA SERVER @addRegistration()
	private void newRegistration() {
		cleanDB();
		addUtentiAmministratori();
		service.addRegistration("NorwegianGoat", "Rick", "Micolli", "2349871013", "Test1234", "linux@ca.com",
				"ABCDEF98G23H567I", "Via Del Cinghiale 56, Bolotown", "M", "23/10/1998", "Firenze");
		service.addRegistration("Afrika90", "Matte", "Triumvirati", "3678364910", "Test2345", "mat@iloud.com",
				"AZBTUI90R06A924U", "Via Del Pastore 82, Firenze", "M", "06/07/1990", "Casalecchio di Reno");
		service.addRegistration("lore", "Lore", "Fissagatti", "0284652822", "Test3456", "lorenz@ping.it",
				"SFBDMO98F01A984I", "Via Lupo 101, Milano", "M", "01/04/1998", "Roma");
		service.addRegistration("benny", "Benny", "Bagnacci", "0825720442", "Chirotteri!", "bb@science.xyz",
				"UWPORT95R02A123U", "Via Collinare 206, Bologna", "F", "02/07/1995", "Ferrara");
		service.addRegistration("MrX", "Odoardo", "Boschetti", "9999999999", "BOSCHETTI", "ob@x.zzz", "OBOARD00R00A00U",
				"Via Del Mistero 33, Chieti", "", "", "");
	}

	// PULIZIA COLLEZIONI DI DATI ESISTENTI NEL DB
	private void cleanDB() {
		mapUtenti.clear();
		mapEmailRegistrate.clear();
		mapCFRegistrati.clear();
		mapSpeziale.clear();
	}

	// INSERIMENTO NEL DB DI NUOVI UTENTI AMMINISTRATORI RAMITE PROCEDURA SERVER
	// @addRegistration()
	private void addUtentiAmministratori() {
		service.addRegistration("Admin", "", "", "", "Admin", "admin@g.com", "", "", "", "", "");
		service.addRegistration("Riccardo", "", "", "", "Test1234", "rick@g.com", "", "", "", "", "");
		service.addRegistration("Matteo", "", "", "", "Test1234", "matte@g.com", "", "", "", "", "");
		service.addRegistration("Lorenzo", "", "", "", "Test1234", "lore@g.com", "", "", "", "", "");
	}

	// TEST INSERIMENTO DI NUOVI UTENTI AMMINISTRATORI E REGISTRATI CON EMAIL, CF e
	// USERNAME GIA' IN USO
	private void registrationReusingData() {
		try {
			service.addRegistration("lore", "Lorenz", "Mistichelli", "2458752589", "Test3", "l@pong.it",
					"OLMOAZ98F21A984L", "Via Luppolo 11, Lucca", "M", "07/05/1979", "Ravenna");
			fail("L'username e' gia' stato usato!");
		} catch (IllegalArgumentException e) {

		}
		try {
			service.addRegistration("Carlos", "Carlo", "S", "0284598490", "Test1", "linux@ca.com", "CLASOO98G23H567I",
					"Via Pelota 86, Brazil", "M", "24/11/1999", "");
			fail("La mail e' gia' stata usata!");
		} catch (IllegalArgumentException e) {

		}

		try {
			service.addRegistration("MartaX", "Marta", "U", "3678364910", "Test2", "marta@iloud.com",
					"AZBTUI90R06A924U", "Via Casale 98, San Lazzaro", "A", "", "Venezia");
			fail("Il cf e' gia' stato usato!");
		} catch (IllegalArgumentException e) {

		}

		// TEST COMPORTAMENTO NUOVA REGISTRAZIONE UTENTE AMMINISTRATORE CON USERNAME
		// GIA' ESISTENTE
		try {
			service.addRegistration("Riccardo", "", "", "", "Test1234", "rick1@g.com", "", "", "", "", "");
			fail("Lo username per il nuovo utente admin è già stato utilizzato!");
		} catch (IllegalArgumentException e) {

		}

		// TEST COMPORTAMENTO NUOVA REGISTRAZIONE UTENTE AMMINISTRATORE CON EMAIL GIA'
		// ESISTENTE
		try {
			service.addRegistration("Riccardo1", "", "", "", "Test1234", "rick@g.com", "", "", "", "", "");
			fail("La email per il nuovo utente admin e' gia' stata utilizzata!");
		} catch (IllegalArgumentException e) {

		}

		// VERIFICA CHE LE ISTANZE DEGLI UTENTI MEMORIZZATE NEL DB SIANO QUELLE GIUSTE
		verificaIstanze();
	}

	// TEST DI VERIFICA SUI TIPI DI ISTANZE UTENTE SALVATI NEL DB:
	// verifica che gli amministratori siano istanze di utenti amministratori e che
	// gli utenti registrati siano istanze di utenti registrati
	private void verificaIstanze() {

		// VERIFICA ISTANZE UTENTI AMMINISTRATORI
		assertTrue(mapUtenti.get("Admin") instanceof UtenteAmministratore);
		assertTrue(mapUtenti.get("Matteo") instanceof UtenteAmministratore);
		assertTrue(mapUtenti.get("Riccardo") instanceof UtenteAmministratore);

		assertFalse(mapUtenti.get("Lorenzo") instanceof UtenteRegistrato);

		// VERIFICA ISTANZE UTENTI REGISTRATI
		assertTrue(mapUtenti.get("NorwegianGoat") instanceof UtenteRegistrato);
		assertTrue(mapUtenti.get("Afrika90") instanceof UtenteRegistrato);
		assertTrue(mapUtenti.get("lore") instanceof UtenteRegistrato);
		assertTrue(mapUtenti.get("benny") instanceof UtenteRegistrato);

		assertFalse(mapUtenti.get("MrX") instanceof UtenteAmministratore);
	}

	@Test
	public void testAll() {
		newRegistration();
		registrationReusingData();
	}
}
