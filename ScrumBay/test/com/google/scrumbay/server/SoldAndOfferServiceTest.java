package com.google.scrumbay.server;

import static org.junit.Assert.fail;

import java.util.LinkedList;

import org.junit.Test;

import com.google.gwt.scrumbay.server.SoldAndOfferServiceImpl;
import com.google.gwt.scrumbay.server.UtenteFactory;
import com.google.gwt.scrumbay.shared.Offerta;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;

public class SoldAndOfferServiceTest {

	private SoldAndOfferServiceImpl service = new SoldAndOfferServiceImpl();

	private void cleanDB() {
		DoOfferServiceTest doOfferService = new DoOfferServiceTest();
		doOfferService.testAll();
	}

	private void getMySoldObjectsTest() {
		UtenteRegistrato seller = (UtenteRegistrato) UtenteFactory.getUtente("NorwegianGoat", "Rick", "Micolli",
				"2349871013", "Test1", "linux@ca.com", "ABCDEF98G23H567I", "Via Del Cinghiale 56, Bolotown", "M",
				"23/10/1998", "Firenze");
		LinkedList<Oggetto> mySoldObjects = service.getMySoldObjects(seller);
		for (Oggetto object : mySoldObjects) {
			if (!object.getVenditore().getUsername().equals(seller.getUsername())) {
				fail("Sono stati ritornati oggetti non venduti da questo utente");
			}
		}
	}

	private void getObjectsIBidOn() {
		UtenteRegistrato buyer = (UtenteRegistrato) UtenteFactory.getUtente("Afrika90", "Matte", "Triumvirati",
				"3678364910", "Test2", "mat@iloud.com", "AZBTUI90R06A924U", "Via Del Pastore 82, Firenze", "M",
				"06/07/1990", "Casalecchio di Reno");
		LinkedList<Oggetto> objectsIBidOn = service.getObjectsWithMyBid(buyer);
		for (Oggetto object : objectsIBidOn) {
			boolean found = false;
			for (Offerta offer : object.getOfferte()) {
				if (offer.getAcquirente().getUsername().equals(buyer.getUsername())) {
					found = true;
					break;
				}
			}
			if (found == false) {
				fail("E' stato restituito un oggetto che non ha offerte di questo utente");
			}
		}
	}

	@Test
	public void testAll() {
		cleanDB();
		getMySoldObjectsTest();
		getObjectsIBidOn();
	}
}
