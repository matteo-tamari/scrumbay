package com.google.scrumbay.server;

import org.mapdb.DB;
import static org.junit.jupiter.api.Assertions.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.SaleServiceImpl;
import com.google.gwt.scrumbay.server.UtenteFactory;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.UtenteVenditore;

class VenditaOggettoTest {

	private SaleServiceImpl vendita = new SaleServiceImpl();
	private DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();
	@SuppressWarnings("unchecked")
	private HTreeMap<Date, Oggetto> mapOggettiInVendita = db
			.hashMap("OggettiInVendita", Serializer.DATE, Serializer.JAVA).counterEnable().createOrOpen();

	private void cleanDB() {
		mapOggettiInVendita.clear();
	}

	private void newVenditaOggetto() {
		CategoryServiceTest category = new CategoryServiceTest();
		category.inserimentoNuoveCategorie();
		Date today = new Date();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 1);
		String tomorrowAsString = format.format(cal.getTime());
		Categoria abbigliamento = mapCategorie.get("Vestiti");

		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("NorwegianGoat", "Rick", "Micolli",
				"2349871013", "Test1234", "linux@ca.com", "ABCDEF98G23H567I", "Via Del Cinghiale 56", "M", "23/10/1998",
				"Firenze");
		UtenteVenditore user1 = new UtenteVenditore(user);
		vendita.sellItem(user1, "Trussardi", "un bellissimo gioiello", 45.67, tomorrowAsString, abbigliamento);
		vendita.sellItem(user1, "Camicia Elegante", "Fantastica camicia per uscite di gala, regalata!", 150,
				tomorrowAsString, abbigliamento);

		user = (UtenteRegistrato) UtenteFactory.getUtente("lore", "Lore", "Fissagatti", "0284652822", "Test3",
				"lorenz@ping.it", "SFBDMO98F01A984I", "Via Lupo 101, Milano", "M", "01/04/1998", "Roma");
		UtenteVenditore user2 = new UtenteVenditore(user);
		vendita.sellItem(user2, "Camicia boschiva", "Camicia Arbecombie come nuova!", 25.00, tomorrowAsString,
				abbigliamento);

		user = (UtenteRegistrato) UtenteFactory.getUtente("Afrika90", "Matte", "Triumvirati", "3678364910", "Test2",
				"mat@iloud.com", "AZBTUI90R06A924U", "Via Del Pastore 82, Firenze", "M", "06/07/1990",
				"Casalecchio di Reno");

	}

	private void venditaOggettoReusingData() {
		Date today = new Date();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 1);
		String tomorrowAsString = format.format(cal.getTime());

		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("NorwegianGoat", "Rick", "Micolli",
				"2349871013", "Test1234", "linux@ca.com", "ABCDEF98G23H567I", "Via Del Cinghiale 56", "M", "23/10/1998",
				"Firenze");
		UtenteVenditore user1 = new UtenteVenditore(user);
		try {
			Categoria abbigliamento = mapCategorie.get("Vestiti");
			vendita.sellItem(user1, "Trussardi", "un bellissimo gioiello", 45.67, tomorrowAsString, abbigliamento);
			fail("L'oggetto e' stato messo in vendita");
		} catch (IllegalArgumentException e) {
		}
	}

	private void venditaOggettoTestDate() {
		Date today = new Date();
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String todayAsString = format.format(today);
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		cal.add(Calendar.DATE, 1);
		String tomorrowAsString = format.format(cal.getTime());
		cal.setTime(today);
		cal.add(Calendar.DATE, -1);
		String yesterdayAsString = format.format(cal.getTime());
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("lore", "Lore", "Fissagatti", "0284652822",
				"Test3", "lorenz@ping.it", "SFBDMO98F01A984I", "Via Lupo 101, Milano", "M", "01/04/1998", "Roma");
		Categoria abbigliamento = mapCategorie.get("Vestiti");
		UtenteVenditore user2 = new UtenteVenditore(user);
		try {
			vendita.sellItem(user2, "Supreme", "Bellissima maglietta ", 578.56, yesterdayAsString, abbigliamento);
			fail("Oggetto e' stato messo in vendita con data scadenza passata");
		} catch (IllegalArgumentException e) {
		}
		try {
			vendita.sellItem(user2, "Supreme", "Bellissima maglietta", 578.56, todayAsString, abbigliamento);
			fail("L'oggetto e' stato messo in vendita con scadenza ad oggi");
		} catch (IllegalArgumentException e) {
		}
		try {
			vendita.sellItem(user2, "Supreme", "Bellissima maglietta", 578.56, tomorrowAsString, abbigliamento);
		} catch (IllegalArgumentException e) {
			fail("L'oggetto non e' stato messo in vendita");
		}

	}

	@Test
	public void testAll() {
		cleanDB();
		newVenditaOggetto();
		venditaOggettoReusingData();
		venditaOggettoTestDate();
	}

}
