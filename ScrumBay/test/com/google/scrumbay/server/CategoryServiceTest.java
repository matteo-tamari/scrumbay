package com.google.scrumbay.server;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.CategoryServiceImpl;
import com.google.gwt.scrumbay.shared.Categoria;

public class CategoryServiceTest {

	private static final String SERVICE_MAP_CATEGORIA = "CategorieOggetti";
	CategoryServiceImpl nuovaCategoriaSvc = new CategoryServiceImpl();
	DB db = DbStarter.getInstance();

	@SuppressWarnings("unchecked")
	HTreeMap<String, Categoria> mapCategorie = db.hashMap(SERVICE_MAP_CATEGORIA, Serializer.STRING, Serializer.JAVA)
			.counterEnable().createOrOpen();

	private void cleanDB() {
		mapCategorie.clear();
	}

	private void categorieSbagliate() {
		try {
			nuovaCategoriaSvc.addNewCategory("", "Modellismo");
			fail("La categoria Padre non dovrebbe esistere!");
		} catch (Exception e) {
		}
		try {
			nuovaCategoriaSvc.addNewCategory("NULL", "Auto");
			fail("La nuova categoria dovrebbe già esistere!");
		} catch (Exception e) {
		}
		try {
			nuovaCategoriaSvc.addNewCategory("Auto", "Pantaloni");
			fail("La nuova categoria dovrebbe già esistere!");
		} catch (Exception e) {
		}
	}

	private void categorieCorrette() {

		LinkedList<Categoria> categorie = nuovaCategoriaSvc.getCategories("NULL");
		assertTrue(categorie.size() == 0);

		try {
			nuovaCategoriaSvc.addNewCategory("NULL", "Vestiti");
			nuovaCategoriaSvc.addNewCategory("NULL", "Auto");
			nuovaCategoriaSvc.addNewCategory("NULL", "Laptop");
			nuovaCategoriaSvc.addNewCategory("Vestiti", "Pantaloni");
			nuovaCategoriaSvc.addNewCategory("Auto", "Fiat");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	private void getCategories() {
		LinkedList<Categoria> categorie = nuovaCategoriaSvc.getCategories("NULL");
		assertTrue(categorie.size() == 3);
		assertTrue(categorie.get(0).getNomeCategoria().equals("Vestiti"));
		assertTrue(categorie.get(1).getNomeCategoria().equals("Auto"));
		assertTrue(categorie.get(2).getNomeCategoria().equals("Laptop"));
		assertFalse(categorie.get(0).getNomeCategoria().equals("Pantaloni"));
		assertFalse(categorie.get(1).getNomeCategoria().equals("Fiat"));

		categorie = nuovaCategoriaSvc.getCategories("Auto");
		assertTrue(categorie.size() == 1);
		assertTrue(categorie.get(0).getNomeCategoria().equals("Fiat"));
		assertFalse(categorie.get(0).getNomeCategoria().equals("Pantaloni"));
	}

	@Test
	void inserimentoNuoveCategorie() {
		cleanDB();
		categorieCorrette();
		categorieSbagliate();
		getCategories();
	}
}
