package com.google.scrumbay.server;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gwt.scrumbay.server.LoginServiceImpl;
import com.google.gwt.scrumbay.shared.UtenteAmministratore;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;

class LoginServiceTest {

	LoginServiceImpl service = new LoginServiceImpl();
	RegistrationServiceTest registration = new RegistrationServiceTest();

	private void loginWithCorrectCredentials() {
		registration.testAll();
		// LOGIN CON CREDENZIALI GIUSTE UTENTE REGISTRATO
		// Verifica che l'istanza di ritorno con le giuste credenziali sia
		// effettivamente di UtenteRegistarto
		assertTrue(service.signIn("NorwegianGoat", "Test1234") instanceof UtenteRegistrato);
		assertTrue(service.signIn("Afrika90", "Test2345") instanceof UtenteRegistrato);
		// Verifica che l'istanza di ritorno con le giuste credenziali non sia un
		// istanza UtenteAmministartore
		assertFalse(service.signIn("lore", "Test3456") instanceof UtenteAmministratore);
		assertFalse(service.signIn("benny", "Chirotteri!") instanceof UtenteAmministratore);

		// LOGIN CON CREDENZIALI GIUSTE UTENTE AMMINISTARTORE
		// Verifica che l'istanza di ritorno con le giuste credenziali sia
		// effettivamente di UtenteAmministratore
		assertTrue(service.signIn("Admin", "Admin") instanceof UtenteAmministratore);
		assertTrue(service.signIn("Lorenzo", "Test1234") instanceof UtenteAmministratore);
		// Verifica che l'istanza di ritorno con le giuste credenziali non sia un
		// istanza UtenteRegistrato
		assertFalse(service.signIn("Matteo", "Test1234") instanceof UtenteRegistrato);
		assertFalse(service.signIn("Riccardo", "Test1234") instanceof UtenteRegistrato);
	}

	private void loginWithIncorrectCredentials() {
		// LOGIN CON CREDENZIALI SBAGLIATE UTENTE REGISTRATO
		try {
			service.signIn("norwegianGoat", "Test1");
			fail("Il login e' andato a buon fine con username sbagliato");
		} catch (IllegalArgumentException e) {

		}
		try {
			service.signIn("NorwegianGoat", "Test2");
			fail("Il login e' andato a buon fine con pw sbagliata");
		} catch (IllegalArgumentException e) {

		}
		try {
			service.signIn("norwegianGoat", "Test3");
			fail("Il login e' andato a buon fine con un username e una pw sbagliati");
		} catch (IllegalArgumentException e) {

		}

		// LOGIN CON CREDENZIALI SBAGLIATE UTENTE AMMINISTRATORE
		try {
			service.signIn("Admin", "a");
		} catch (IllegalArgumentException e) {

		}
		try {
			service.signIn("Aaaaa", "Admin");
		} catch (IllegalArgumentException e) {

		}
		try {
			service.signIn("Aaaaa", "www");
		} catch (IllegalArgumentException e) {

		}
	}

	@Test
	public void testAll() {
		loginWithCorrectCredentials();
		loginWithIncorrectCredentials();
	}

}
