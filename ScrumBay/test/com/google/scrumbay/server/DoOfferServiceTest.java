package com.google.scrumbay.server;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import com.google.gwt.scrumbay.server.DbStarter;
import com.google.gwt.scrumbay.server.DoOfferServiceImpl;
import com.google.gwt.scrumbay.server.OggettoFactory;
import com.google.gwt.scrumbay.server.SaleServiceImpl;
import com.google.gwt.scrumbay.server.UtenteFactory;
import com.google.gwt.scrumbay.shared.Categoria;
import com.google.gwt.scrumbay.shared.Oggetto;
import com.google.gwt.scrumbay.shared.UtenteAcquirente;
import com.google.gwt.scrumbay.shared.UtenteRegistrato;
import com.google.gwt.scrumbay.shared.UtenteVenditore;

class DoOfferServiceTest {

	private CategoryServiceTest categoryTest = new CategoryServiceTest();
	private SaleServiceImpl saleService = new SaleServiceImpl();
	private DoOfferServiceImpl doOfferService = new DoOfferServiceImpl();
	private UtenteVenditore venditore;
	private Oggetto oggetto = null;
	private DB db = DbStarter.getInstance();
	@SuppressWarnings("unchecked")
	private HTreeMap<String, Categoria> mapCategorie = db
			.hashMap("CategorieOggetti", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();

	private void cleanDB() {
		@SuppressWarnings("unchecked")
		HTreeMap<String, Oggetto> mapOggettiInVendita = db
				.hashMap("OggettiInVendita", Serializer.STRING, Serializer.JAVA).counterEnable().createOrOpen();
		mapOggettiInVendita.clear();
		categoryTest.inserimentoNuoveCategorie();
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("NorwegianGoat", "Rick", "Micolli",
				"2349871013", "Test1", "linux@ca.com", "ABCDEF98G23H567I", "Via Del Cinghiale 56, Bolotown", "M",
				"23/10/1998", "Firenze");
		venditore = new UtenteVenditore(user);
		saleService.sellItem(venditore, "Sciarpa", "Una semplice sciarpa", 10, "18/6/2021",
				mapCategorie.get("Vestiti"));
	}

	private void doOffer() {
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("Afrika90", "Matte", "Triumvirati",
				"3678364910", "Test2", "mat@iloud.com", "AZBTUI90R06A924U", "Via Del Pastore 82, Firenze", "M",
				"06/07/1990", "Casalecchio di Reno");
		UtenteAcquirente acquirente = new UtenteAcquirente(user);
		oggetto = OggettoFactory.getOggetto(venditore, "Sciarpa", "Una semplice sciarpa", 10, "18/6/2021",
				mapCategorie.get("Vestiti"));
		doOfferService.offer(acquirente, 11, oggetto);
	}

	private void doEqualOffer() {
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("lore", "Lore", "Fissagatti", "0284652822",
				"Test3", "lorenz@ping.it", "SFBDMO98F01A984I", "Via Lupo 101, Milano", "M", "01/04/1998", "Roma");
		UtenteAcquirente acquirente = new UtenteAcquirente(user);
		try {
			doOfferService.offer(acquirente, 11, oggetto);
			fail("E' stata accettata una offeta uguale alla attuale vincente");
		} catch (IllegalArgumentException e) {
		}

	}

	private void doLowerOffer() {
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("benny", "Benny", "Bagnacci", "0825720442",
				"Chirotteri!", "bb@science.xyz", "UWPORT95R02A123U", "Via Collinare 206, Bologna", "F", "02/07/1995",
				"Ferrara");
		UtenteAcquirente acquirente = new UtenteAcquirente(user);
		try {
			doOfferService.offer(acquirente, 10, oggetto);
			fail("E' stata accettata una offerta inferiore alla attuale vincente");
		} catch (IllegalArgumentException e) {
		}
	}

	private void doHigherOffer() {
		UtenteRegistrato user = (UtenteRegistrato) UtenteFactory.getUtente("MrX", "Odoardo", "Boschetti", "9999999999",
				"BOSCHETTI", "ob@x.zzz", "OBOARD00R00A00U", "Via Del Mistero 33, Chieti", "", "", "");
		UtenteAcquirente acquirente = new UtenteAcquirente(user);
		try {
			doOfferService.offer(acquirente, 12, oggetto);
		} catch (IllegalArgumentException e) {
			fail("E' stata rifiutata una offerta maggiore alla attuale vincente");
		}
	}

	@Test
	void testAll() {
		cleanDB();
		doOffer();
		doEqualOffer();
		doLowerOffer();
		doHigherOffer();
	}
}
